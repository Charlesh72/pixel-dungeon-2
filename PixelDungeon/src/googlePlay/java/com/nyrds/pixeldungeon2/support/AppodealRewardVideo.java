/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.support;

import android.os.Build;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.RewardedVideoCallbacks;
import com.appodeal.ads.utils.Log;
import com.nyrds.pixeldungeon2.ml.BuildConfig;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.noosa.InterstitialPoint;
import com.watabou.pixeldungeon.PixelDungeon;
import com.watabou.pixeldungeon.utils.GLog;

/**
 * Created by mike on 18.02.2017.
 * This file is part of Pixel Dungeon 2.
 */

public class AppodealRewardVideo {
	private static InterstitialPoint returnTo;


	private static boolean isAllowed() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
	}

	public static void initCinemaRewardVideo() {

		if (!isAllowed()) {
			return;
		}

		Game.instance().runOnUiThread(new Runnable() {
			@Override
			public void run() {

				String appKey = Game.getVar(R.string.appodealRewardAdUnitId);

				String disableNetworks[] = {"facebook","flurry","startapp","avocarrot","ogury"};

				for(String net:disableNetworks) {
					Appodeal.disableNetwork(PixelDungeon.instance(), net);
				}
				Appodeal.disableLocationPermissionCheck();

				if(BuildConfig.DEBUG) {
					Appodeal.setLogLevel(Log.LogLevel.verbose);
					//Appodeal.setTesting(true);
				}

				Appodeal.initialize(PixelDungeon.instance(), appKey, Appodeal.REWARDED_VIDEO);
				Appodeal.setRewardedVideoCallbacks(new RewardedVideoCallbacks() {
					@Override
					public void onRewardedVideoLoaded() {
						if(BuildConfig.DEBUG) {GLog.i("onRewardedVideoLoaded");}
					}
					@Override
					public void onRewardedVideoFailedToLoad() {
						if(BuildConfig.DEBUG) {GLog.i("onRewardedVideoFailedToLoad");}
					}
					@Override
					public void onRewardedVideoShown() {
						if(BuildConfig.DEBUG) {GLog.i("onRewardedVideoShown");}
					}
					@Override
					public void onRewardedVideoFinished(int amount, String name) {
						if(BuildConfig.DEBUG) {GLog.i("onRewardedVideoFinished. Reward: %d %s", amount, name);}
					}
					@Override
					public void onRewardedVideoClosed(final boolean finished) {
						if(BuildConfig.DEBUG) {GLog.i("onRewardedVideoClosed,  finished: %s", finished);}
						//PixelDungeon.immerse(PixelDungeon.immersed());
						returnTo.returnToWork(finished);
					}
				});
			}
		});
	}

	public static void showCinemaRewardVideo(InterstitialPoint ret) {
		returnTo = ret;
		Game.instance().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(isReady()) {
					Appodeal.show(PixelDungeon.instance(), Appodeal.REWARDED_VIDEO);
				} else {
					returnTo.returnToWork(false);
				}
			}
		});
	}

	public static boolean isReady() {
		return isAllowed() && Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
	}
}
