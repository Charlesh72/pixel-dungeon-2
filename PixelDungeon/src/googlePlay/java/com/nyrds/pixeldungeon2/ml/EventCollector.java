/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.ml;

import android.content.Context;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nyrds.android.util.Util;
import com.watabou.pixeldungeon.Preferences;

import org.acra.ACRA;

import java.util.HashMap;

/**
 * Created by mike on 09.03.2016.
 */
public class EventCollector {
	public static final String BUG = "bug";

	static private Tracker mTracker;
	static private boolean mDisabled = true;

	static private HashMap<String, Long> timings;

	private static boolean googleAnalyticsUsable() {
		return Preferences.INSTANCE.getInt(Preferences.KEY_COLLECT_STATS,1) > 0;
	}

	static public void init(Context context) {
		// TODO: 1/2/2018 Set up event collecter id in app_tracker.xml
		if (false && mTracker == null) {

			if(!googleAnalyticsUsable()) {
				EventCollector.disable();
				return;
			}

			AnalyticsTrackers.initialize(context);

			mTracker = AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
			mTracker.enableAdvertisingIdCollection(true);
			mDisabled = false;
		}
	}

	static public void disable() {
		mDisabled = true;
	}

	static public void logEvent(String category, String event) {
		if (!mDisabled) {
			mTracker.send(new HitBuilders.EventBuilder().setCategory(category).setAction(event).build());
		}
	}

	static public void logEvent(String category, String event, String label) {
		if (!mDisabled) {
			mTracker.send(new HitBuilders.EventBuilder().setCategory(category).setAction(event).setLabel(label).build());
		}
	}

	static public void logScene(String scene) {
		if (!mDisabled) {
			mTracker.setScreenName(scene);
			mTracker.send(new HitBuilders.ScreenViewBuilder().build());
		}
	}

	static public void logException(Exception e) {
		if(!mDisabled) {
			mTracker.send(new HitBuilders.ExceptionBuilder().setDescription(Util.toString(e)).build());
			e.printStackTrace();
		}
	}

	static public void logException(Exception e,String desc) {
		if(!mDisabled) {
			mTracker.send(new HitBuilders.ExceptionBuilder().setDescription(desc + " "+Util.toString(e)).build());
			e.printStackTrace();
		}
	}

	static public void startTiming(String id) {

	}

	static public void stopTiming(String id) {

	}

	public static void collectSessionData(String key, String value) {
		if(ACRA.isInitialised()) {
			ACRA.getErrorReporter().putCustomData(key, value);
		}
	}
}
