/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.ui;

import com.nyrds.pixeldungeon2.support.PlayGames;
import com.watabou.noosa.Game;
import com.watabou.noosa.Image;
import com.watabou.noosa.ui.Button;
import com.watabou.utils.SystemTime;

public class PlayGamesButton extends Button {

	private Image image;
	private long lastUpdatedTime;

	public PlayGamesButton() {
		super();
		
		width = image.width;
		height = image.height;
	}

	private void updateStatus() {
		if(PlayGames.isConnected()) {
			image.brightness(1.5f);
		} else {
			image.brightness(0.5f);
		}
	}

	@Override
	protected void createChildren() {
		super.createChildren();
		
		image = Icons.get(Icons.PLAY_GAMES);

		updateStatus();

		add( image );
	}
	
	@Override
	protected void layout() {
		super.layout();

		image.x = x;
		image.y = y;
	}
	
	@Override
	public void update() {
		super.update();
		if(SystemTime.now() - lastUpdatedTime > 1000) {
			lastUpdatedTime = SystemTime.now();
			updateStatus();
		}
	}

	@Override
	protected void onClick() {
		Game.scene().add(new WndPlayGames());
	}
}
