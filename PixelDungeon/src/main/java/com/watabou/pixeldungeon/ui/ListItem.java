/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.ui;

import com.nyrds.android.util.GuiProperties;
import com.watabou.noosa.CompositeTextureImage;
import com.watabou.noosa.Text;
import com.watabou.noosa.ui.Component;
import com.watabou.pixeldungeon.scenes.PixelScene;

/**
 * Created by mike on 15.05.2017.
 * This file is part of Pixel Dungeon 2.
 */
public abstract class ListItem extends Component implements IClickable {

	protected CompositeTextureImage sprite    = new CompositeTextureImage();
	protected Text                  label     = PixelScene.createText(GuiProperties.regularFontSize());
	protected boolean               clickable = false;
	protected int                   align     = 24;

	protected ListItem() {
		super();
		add(sprite);
		add(label);
	}

	@Override
	protected void layout() {
		sprite.y = PixelScene.align(y + (height - sprite.height) / 2);

		label.x = Math.max(sprite.x + sprite.width, align);
		label.y = PixelScene.align(y + (height - label.baseLine()) / 2);
	}

	public boolean onClick(float x, float y) {
		if (clickable && inside(x, y)) {
			onClick();
			return true;
		} else {
			return false;
		}
	}

	abstract protected void onClick();
}
