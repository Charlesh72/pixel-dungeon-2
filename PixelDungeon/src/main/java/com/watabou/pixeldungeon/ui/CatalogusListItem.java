/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.ui;

import com.nyrds.pixeldungeon2.ml.EventCollector;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.sprites.ItemSprite;
import com.watabou.pixeldungeon.windows.WndInfoItem;

/**
 * Created by mike on 15.05.2017.
 * This file is part of Pixel Dungeon 2.
 */
public class CatalogusListItem extends ListItem {

	private Item item;

	public CatalogusListItem(Class<? extends Item> cl) {
		super();

		try {
			item = cl.newInstance();
			if (clickable = item.isIdentified()) {
				sprite.copy(new ItemSprite(item));
				label.text(item.name());
			} else {
				sprite.copy(new ItemSprite());
				label.text(item.trueName());
				label.hardlight(0xCCCCCC);
			}
		} catch (Exception e) {
			EventCollector.logException(e);
		}
		add(sprite);
	}

	@Override
	public void onClick() {
		GameScene.show(new WndInfoItem(item));
	}
}
