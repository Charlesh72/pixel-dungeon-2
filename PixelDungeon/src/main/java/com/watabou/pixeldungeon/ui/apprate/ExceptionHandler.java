package com.watabou.pixeldungeon.ui.apprate;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Charles on 12/18/2017.
 */

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler defaultExceptionHandler;
    SharedPreferences preferences;

    // Constructor.
    public ExceptionHandler(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Context context)
    {
        preferences = context.getSharedPreferences(PrefsContract.SHARED_PREFS_NAME, 0);
        defaultExceptionHandler = uncaughtExceptionHandler;
    }

    public void uncaughtException(Thread thread, Throwable throwable) {

        preferences.edit().putBoolean(PrefsContract.PREF_APP_HAS_CRASHED, true).commit();

        // Call the original handler.
        defaultExceptionHandler.uncaughtException(thread, throwable);
    }
}
