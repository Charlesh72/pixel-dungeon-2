/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.windows;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.PixelDungeon;
import com.watabou.pixeldungeon.ui.RedButton;
import com.watabou.pixeldungeon.ui.Window;

public class WndPremiumSettings extends Window {

	private static final String NOT_AVALIABLE =  Game.getVar(R.string.WndPremiumSettings_notAvailable);
	
	private static final String TOOLBAR = Game.getVar(R.string.WndPremiumSettings_toolbar);
	private static final String STATUS  = Game.getVar(R.string.WndPremiumSettings_status);
	private static final String CHROME  = Game.getVar(R.string.WndPremiumSettings_chrome);
	private static final String BANNERS = Game.getVar(R.string.WndPremiumSettings_banners);
	
	private static final String RUBY   = Game.getVar(R.string.WndPremiumSettings_ruby);
	private static final String GOLD   = Game.getVar(R.string.WndPremiumSettings_gold);
	private static final String SILVER = Game.getVar(R.string.WndPremiumSettings_silver);
	private static final String STD    = Game.getVar(R.string.WndPremiumSettings_std);
		
	private static final int WIDTH      = 112;

	private int curBottom = 0;

	public WndPremiumSettings() {
		super();
		
		createAssetsSelector("chrome",  CHROME);
		createAssetsSelector("status",  STATUS);
		createAssetsSelector("toolbar", TOOLBAR);
		//Disabled because I don't want to remake the banner for these options
//		createAssetsSelector("banners", BANNERS);
		
		resize(WIDTH, curBottom);
	}

	private void createAssetsSelector(final String assetKind, final String assetName) {
		RedButton btn = new RedButton(assetName) {
			@Override
			protected void onClick() {
				PixelDungeon.scene().add(
						new WndOptions(assetName, "", STD, SILVER,
								GOLD, RUBY) {
							@Override
							protected void onSelect(int index) {
								if (PixelDungeon.donated() >= index) {
									Assets.use(assetKind, index);
									PixelDungeon.scene().add(
											new WndMessage("ok!"));
								} else {
									PixelDungeon.scene().add(
											new WndMessage(NOT_AVALIABLE));
								}
							}
						});
			}
		};

		btn.setRect(0, curBottom, WIDTH, BUTTON_HEIGHT);
		add(btn);
		curBottom += BUTTON_HEIGHT + GAP;
	}
	
	@Override
	public void onBackPressed() {
		hide();
		PixelDungeon.resetScene();
	}
}
