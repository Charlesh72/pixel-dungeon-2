/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.windows.elements;

import com.watabou.noosa.Image;
import com.watabou.noosa.ui.Button;
import com.watabou.pixeldungeon.Assets;

public class Tool extends Button {

	private static final int BGCOLOR = 0x7B8073;

	private Image base;

	public Tool(int x, int y, int width, int height) {
		super();

		base.frame(x, y, width, height);

		this.width = width;
		this.height = height;
	}

	@Override
	protected void createChildren() {
		super.createChildren();

		base = new Image(Assets.getToolbar());
		add(base);
	}

	@Override
	protected void layout() {
		super.layout();

		base.x = x;
		base.y = y;
	}

	@Override
	protected void onTouchDown() {
		base.brightness(1.4f);
	}

	@Override
	protected void onTouchUp() {
		if (active) {
			base.resetColor();
		} else {
			base.tint(BGCOLOR, 0.7f);
		}
	}

	public void enable(boolean value) {
		if (value != active) {
			if (value) {
				base.resetColor();
			} else {
				base.tint(BGCOLOR, 0.7f);
			}
			active = value;
		}
	}
}