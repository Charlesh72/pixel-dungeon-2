/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.windows;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.ui.RedButton;
import com.watabou.pixeldungeon.ui.Window;

/**
 * Created by mike on 16.04.2017.
 * This file is part of Pixel Dungeon 2.
 */

public class Selector {

	private int width, height;

	private Window parent;

	private RedButton btnPlus;
	private RedButton btnMinus;
	private RedButton btnDefault;

	private static final String TXT_PLUS      = Game
			.getVar(R.string.WndSettings_ZoomIn);
	private static final String TXT_MINUS     = Game
			.getVar(R.string.WndSettings_ZoomOut);

	public Selector(Window wnd, int width, int height) {
		this.width = width;
		this.height = height;
		parent = wnd;
	}

	public void enable(boolean p, boolean m, boolean d) {
		btnDefault.enable(d);
		btnMinus.enable(m);
		btnPlus.enable(p);
	}

	public void enable(boolean val) {
		enable(val,val,val);
	}

	public void remove() {
		parent.remove(btnPlus);
		parent.remove(btnMinus);
		parent.remove(btnDefault);
	}

	public void setText(String text) {
		btnDefault.text(text);
	}

	public float add(float y,String text, final PlusMinusDefault actions) {
		int w = height;

		btnPlus = new RedButton(TXT_PLUS) {
			@Override
			protected void onClick() {
				actions.onPlus();
			}
		};
		parent.add(btnPlus.setRect(width - w, y, w, height));

		btnMinus = new RedButton(TXT_MINUS) {
			@Override
			protected void onClick() {
				actions.onMinus();
			}
		};
		parent.add(btnMinus.setRect(0, y, w, height));

		btnDefault = new RedButton(text) {
			@Override
			protected void onClick() {
				actions.onDefault();
			}
		};
		btnDefault.setRect(btnMinus.right(), y, width - btnPlus.width()
				- btnMinus.width(), height);
		parent.add(btnDefault);

		return btnMinus.bottom();
	}

	public interface PlusMinusDefault {
		void onPlus();
		void onMinus();
		void onDefault();
	}
}
