/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.windows.elements;

import com.watabou.noosa.NinePatch;
import com.watabou.noosa.audio.Sample;
import com.watabou.noosa.ui.Button;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.Chrome;
import com.watabou.pixeldungeon.windows.WndTabbed;

public class Tab extends Button {
	
	private final WndTabbed parent;

	protected Tab(WndTabbed wndTabbed) {
		parent = wndTabbed;
	}

	protected final int CUT = 5;
	
	protected boolean selected;

	protected NinePatch bg;
	
	@Override
	protected void layout() {
		super.layout();
		
		if (bg != null) {
			bg.x = x;
			bg.y = y;
			bg.size( width, height );
		}
	}
	
	public void select( boolean value ) {
		
		active = !(selected = value);
		
		if (bg != null) {
			remove( bg );
		}
		
		bg = Chrome.get( selected ? 
			Chrome.Type.TAB_SELECTED : 
			Chrome.Type.TAB_UNSELECTED );
		addToBack( bg );
		
		layout();
	}
	
	@Override
	protected void onClick() {	
		Sample.INSTANCE.play( Assets.SND_CLICK, 0.7f, 0.7f, 1.2f );
		parent.onClick( this );
	}
}