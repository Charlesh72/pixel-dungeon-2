/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.windows.elements;

import com.nyrds.android.util.GuiProperties;
import com.watabou.noosa.Text;
import com.watabou.pixeldungeon.scenes.PixelScene;
import com.watabou.pixeldungeon.windows.WndTabbed;

public class LabeledTab extends Tab {

	private Text btLabel;

	public LabeledTab(WndTabbed parent, String label) {
		super(parent);
		btLabel.text(label);
		btLabel.measure();
	}

	@Override
	protected void createChildren() {
		super.createChildren();

		btLabel = PixelScene.createText(GuiProperties.titleFontSize());
		add(btLabel);
	}

	@Override
	protected void layout() {
		super.layout();

		btLabel.x = PixelScene.align(x + (width  - btLabel.width()) / 2);
		btLabel.y = PixelScene.align(y + (height - btLabel.baseLine()) / 2) - 1;
		if (!selected) {
			btLabel.y -= 2;
		}
	}

	@Override
	public void select(boolean value) {
		super.select(value);
		btLabel.am = selected ? 1.0f : 0.6f;
	}
}