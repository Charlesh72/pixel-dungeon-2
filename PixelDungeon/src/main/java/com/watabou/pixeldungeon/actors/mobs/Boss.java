/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.actors.mobs;

import com.nyrds.android.util.TrackedRuntimeException;
import com.watabou.noosa.audio.Music;
import com.watabou.pixeldungeon.items.scrolls.ScrollOfPsionicBlast;
import com.watabou.pixeldungeon.items.weapon.enchantments.Death;
import com.watabou.pixeldungeon.scenes.GameScene;

import org.json.JSONException;
import org.json.JSONObject;

abstract public class Boss extends Mob {

	private static final String BATTLE_MUSIC = "battleMusic";
	private String battleMusic;

	public Boss() {
		RESISTANCES.add(Death.class);
		RESISTANCES.add(ScrollOfPsionicBlast.class);
		maxLvl = 50;
	}

	@Override
	public boolean canBePet() {
		return false;
	}

	@Override
	public void setState(AiState state) {
		if (state instanceof Hunting) {
			if (battleMusic != null) {
				Music.INSTANCE.play(battleMusic, true);
			}
		}
		super.setState(state);
	}

	@Override
	public void die(Object cause) {
		GameScene.playLevelMusic();
		super.die(cause);
	}

	@Override
	protected void readCharData() {
		super.readCharData();
		try {
			JSONObject desc = getClassDef();

			if (desc.has(BATTLE_MUSIC)) {
				battleMusic = desc.getString(BATTLE_MUSIC);
			}
		} catch (JSONException e) {
			throw new TrackedRuntimeException(e);
		}

	}
}
