/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.actors.blobs;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.effects.BlobEmitter;
import com.watabou.pixeldungeon.effects.particles.DarknessParticle;

public class Darkness extends Blob {
	
	@Override
	protected void evolve() {
		int from = getWidth() + 1;
		int to   = getLength() - getWidth() - 1;

		for (int pos=from; pos < to; pos++) {
			if (cur[pos] > 0) {
				off[pos] = cur[pos];
				volume += off[pos];
			} else {
				off[pos] = 0;
			}
		}
	}
	
	@Override
	public void use( BlobEmitter emitter ) {
		super.use( emitter );	
		emitter.start(DarknessParticle.FACTORY, 0.9f, 0 );
	}
	
	@Override
	public String tileDesc() {
		return Game.getVar(R.string.Darkness_Info);
	}
}
