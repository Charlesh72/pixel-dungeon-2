/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.actors.mobs;

import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.TerrainFlags;

/**
 * Created by mike on 26.04.2017.
 * This file is part of Pixel Dungeon 2.
 */
public enum WalkingType {
	NORMAL, WATER, WALL, GRASS, CHASM, ABSOLUTE;

	public boolean[] passableCells(Level level) {
		switch (this) {
			case NORMAL:
				return level.passable;
			case WATER:
				return level.water;
			case WALL:
				return level.solid;
			case GRASS:
				return level.passable;
			case CHASM:
				return level.pit;
			case ABSOLUTE:
				return level.allCells;
		}
		return level.passable;
	}

	public boolean canSpawnAt(Level level,int cell) {
		switch (this) {
			case NORMAL:
			case ABSOLUTE:
				return passableCells(level)[cell] || TerrainFlags.is(level.getTileType(cell),TerrainFlags.TRAP);
			default:
				return passableCells(level)[cell];
		}
	}

	public int respawnCell(Level level) {
		switch (this) {
			case NORMAL:
				return level.randomRespawnCell();
			case WATER:
				return level.randomRespawnCell(level.water);
			case WALL:
				return level.randomRespawnCell(level.solid);
			case GRASS:
				return level.randomRespawnCell();
			case CHASM:
				return level.randomRespawnCell(level.pit);
			case ABSOLUTE:
				return level.randomRespawnCell();
			default:
				return level.randomRespawnCell();
		}
	}
}
