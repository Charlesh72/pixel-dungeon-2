/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.scenes;

import com.nyrds.android.util.GuiProperties;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Camera;
import com.watabou.noosa.Game;
import com.watabou.noosa.NinePatch;
import com.watabou.noosa.Text;
import com.watabou.noosa.ui.Component;
import com.watabou.pixeldungeon.Chrome;
import com.watabou.pixeldungeon.PixelDungeon;
import com.watabou.pixeldungeon.Preferences;
import com.watabou.pixeldungeon.ui.Archs;
import com.watabou.pixeldungeon.ui.RedButton;
import com.watabou.pixeldungeon.ui.ScrollPane;
import com.watabou.pixeldungeon.utils.GLog;

public class WelcomeScene extends PixelScene {

	private static final int GAP = 4;

	@Override
	public void create() {
		super.create();

		long start = System.nanoTime();

		String[] upds = {
				Game.getVar(R.string.copyright_notice),
				Game.getVar(R.string.Welcome_Text_v_1_4),
				Game.getVar(R.string.Welcome_Text_v_1_4_1),
				Game.getVar(R.string.Welcome_Text_v_1_4_2),
				Game.getVar(R.string.Welcome_Text_v_1_4_3),
				Game.getVar(R.string.Welcome_Text_v_1_4_4),
				Game.getVar(R.string.Welcome_Text_v_1_4_5),
				Game.getVar(R.string.Welcome_Text_v_1_4_6),
				Game.getVar(R.string.Welcome_Text_v_1_5),
		};

		int displayUpdates = Math.min(upds.length, 5);

		Text[] updTexts = new Text[displayUpdates];

		for (int i = 0; i < displayUpdates; i++) {
			updTexts[i] = createMultiline(GuiProperties.regularFontSize());
		}

		Text title = createMultiline(Game.getVar(R.string.Welcome_Title), GuiProperties.bigTitleFontSize());

		int w = Camera.main.width;
		int h = Camera.main.height;

		int pw = w - 10;

		title.maxWidth(pw);
		title.measure();

		title.x = align((w - title.width()) / 2);
		title.y = align(8);
		add(title);

		NinePatch panel = Chrome.get(Chrome.Type.WINDOW);

		panel.x = (w - pw) / 2;
		panel.y = title.y + title.height() + GAP * 2;
		int ph = (int) (h - panel.y - 22);

		panel.size(pw, ph);

		add(panel);

		ScrollPane list = new ScrollPane(new Component());
		add(list);
		list.setRect(panel.x + panel.marginLeft(), panel.y + panel.marginTop(), panel.innerWidth(),
				panel.innerHeight());
		list.scrollTo(0, 0);

		Component content = list.content();
		content.clear();

		float yPos = 0;
		for (int i = 0; i < displayUpdates; i++) {
			updTexts[i].maxWidth((int) panel.innerWidth());
			updTexts[i].text(upds[upds.length - i - 1]);
			updTexts[i].measure();

			updTexts[i].setPos(0, yPos);
			yPos += updTexts[i].height() + GAP;
			content.add(updTexts[i]);
		}

		content.setSize(panel.innerWidth(), yPos);

		RedButton okay = new RedButton(Game.getVar(R.string.Welcome_Ok)) {
			@Override
			protected void onClick() {
				PixelDungeon.version(Game.versionCode);

				if (Preferences.INSTANCE.getInt(Preferences.KEY_COLLECT_STATS, 1) == 0) {
					Game.switchScene(AllowStatisticsCollectionScene.class);
				} else {
					Game.switchScene(TitleScene.class);
				}
			}
		};

		okay.setRect((w - pw) / 2, h - 22, pw, 18);
		add(okay);

		Archs archs = new Archs();
		archs.setSize(Camera.main.width, Camera.main.height);
		addToBack(archs);

		long end = System.nanoTime();

		GLog.i("Time: %5.3f", (end-start)/100000.f);


		fadeIn();
	}
}
