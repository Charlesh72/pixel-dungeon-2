package com.watabou.pixeldungeon.items.weapon.melee;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.sprites.ItemSpriteSheet;

/**
 * Created by Charles on 12/12/2017.
 */

public class SharpBone extends MeleeWeapon {
    {
        imageFile = "items/daggers.png";
        image = 4;
    }
    public SharpBone() {
        super(1, 1.5f, 1f);
    }

    @Override
    public String desc() {
        return Game.getVar(R.string.SharpBone_Info);
    }
}