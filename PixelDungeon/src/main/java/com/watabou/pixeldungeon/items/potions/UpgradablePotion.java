/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.items.potions;

import com.watabou.pixeldungeon.items.Item;
import com.watabou.utils.Random;

/**
 * Created by mike on 28.05.2017.
 * This file is part of Pixel Dungeon 2.
 */

public class UpgradablePotion extends Potion {
	public UpgradablePotion() {
		super();
	}

	@Override
	public boolean isUpgradable() {
		return true;
	}

	double qualityFactor() {
		return Math.pow(2,level());
	}

	@Override
	public Item upgrade() {
		if(getCurUser()!= null && quantity() > 1) {
			Item potion = detach(getCurUser().belongings.backpack);
			getCurUser().collect(potion.upgrade());
			return this;
		}
		return super.upgrade();
	}

	@Override
	public Item degrade() {

		if(getCurUser()!= null && quantity() > 1) {
			Item potion = detach(getCurUser().belongings.backpack);
			getCurUser().collect(potion.degrade());
			return this;
		}
		return super.degrade();
	}

	@Override
	public int visiblyUpgraded() {
		return level();
	}

	@Override
	public Item random() {
		if (Random.Float() < 0.15f) {
			upgrade();
			if (Random.Float() < 0.15f) {
				upgrade();
			}
		}

		return this;
	}
}
