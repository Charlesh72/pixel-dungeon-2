/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.items.rings;

import com.nyrds.pixeldungeon2.ml.R;

import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.Badges;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.ResultDescriptions;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.sprites.ItemSpriteSheet;
import com.watabou.pixeldungeon.ui.BuffIndicator;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.pixeldungeon.utils.Utils;

public class RingOfStoneWalking extends Artifact{

	public RingOfStoneWalking() {
		image = ItemSpriteSheet.RING_OF_STONE_WALKING;
		identify();
	}
	
	@Override
	protected ArtifactBuff buff( ) {
		return new StoneWalking();
	}
	
	public class StoneWalking extends ArtifactBuff implements Hero.Doom{
		@Override
		public int icon() {
			return BuffIndicator.STONEBLOOD;
		}

		@Override
		public String toString() {
			return Game.getVar(R.string.StoneBlood_Buff);
		}

		@Override
		public void onDeath() {
			Badges.validateDeathInStone();
			
			Dungeon.fail( Utils.format( ResultDescriptions.IMMURED, Dungeon.depth ) );
			GLog.n( Game.getVar(R.string.RingOfStoneWalking_ImmuredInStone));
			
		}
	}
}
