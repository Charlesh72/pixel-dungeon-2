/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.items;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.nyrds.android.util.JsonHelper;
import com.nyrds.android.util.ModdingMode;
import com.watabou.noosa.Game;

public class ItemSpritesDescription {
	private static final String SPRITES_DESC_ITEMS_JSON = "spritesDesc/items.json";

	static private Map<String, ItemSpritesDescription> m_descMap = new HashMap<>();

	private String imageFile;
	private int imageIndex;
	private boolean fliesStraight;
	private boolean fliesFastRotating;

	private ItemSpritesDescription(String imageFile, int imageIndex, boolean fliesStraight, boolean fliesFastRotating) {
		this.imageFile = imageFile;
		this.imageIndex = imageIndex;
		this.fliesStraight = fliesStraight;
		this.fliesFastRotating = fliesFastRotating;
	}

	static public String getImageFile(Item item) {
		ItemSpritesDescription entry = m_descMap.get(item.getClass().getSimpleName());
		if (entry != null) {
			return entry.imageFile;
		}
		return null;
	}

	static public Integer getImageIndex(Item item) {
		ItemSpritesDescription entry = m_descMap.get(item.getClass().getSimpleName());
		if (entry != null) {
			return entry.imageIndex;
		}
		return null;
	}

	static public boolean isFliesStraight(Item item) {
		ItemSpritesDescription entry = m_descMap.get(item.getClass().getSimpleName());
		if (entry != null) {
			return entry.fliesStraight;
		}
		return false;
	}

	static public boolean isFliesFastRotating(Item item) {
		ItemSpritesDescription entry = m_descMap.get(item.getClass().getSimpleName());
		if (entry != null) {
			return entry.fliesFastRotating;
		}
		return false;
	}

	static public void readItemsDesc() {
		if (ModdingMode.isResourceExist(SPRITES_DESC_ITEMS_JSON)) {
			JSONObject itemsDesc = JsonHelper.readJsonFromAsset(SPRITES_DESC_ITEMS_JSON);

			Iterator<?> keys = itemsDesc.keys();

			while (keys.hasNext()) {
				String key = (String) keys.next();
				try {
					JSONObject itemDesc = itemsDesc.getJSONObject(key);

					m_descMap.put(key,
							new ItemSpritesDescription(itemDesc.optString("file", "items.png"),
									itemDesc.optInt("index", 0), 
									itemDesc.optBoolean("fliesStraight", false),
									itemDesc.optBoolean("fliesFastRotating", false)));
				} catch (JSONException e) {
					Game.toast("malformed desc (%s) for %s ignored", itemsDesc.toString(), key);
				}

			}
		}
	}
}