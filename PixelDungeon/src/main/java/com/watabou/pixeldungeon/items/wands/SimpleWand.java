/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.items.wands;

import com.nyrds.android.util.TrackedRuntimeException;
import com.watabou.utils.Random;

public abstract class SimpleWand extends Wand {
	
	@SuppressWarnings("rawtypes")
	private static Class[] variants = {	WandOfAmok.class, 
		WandOfAvalanche.class, 
		WandOfDisintegration.class, 
		WandOfFirebolt.class, 
		WandOfLightning.class,
		WandOfPoison.class, 
		WandOfRegrowth.class, 
		WandOfSlowness.class};
	
	static public SimpleWand createRandomSimpleWand() {
		try {
			return (SimpleWand) Random.element(variants).newInstance();
		} catch (Exception e) {
			throw new TrackedRuntimeException(e);
		}
	}
	
	
}
