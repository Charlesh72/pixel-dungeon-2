/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.items.rings;

import com.nyrds.android.util.TrackedRuntimeException;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.buffs.Buff;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.items.EquipableItem;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.pixeldungeon.utils.Utils;

import java.util.ArrayList;

public class Artifact extends EquipableItem {

	static final float TIME_TO_EQUIP = 1f;
	protected Buff buff;

	@Override
	public boolean doEquip(Hero hero) {
		setCurUser(hero);

		if (hero.belongings.ring1 != null && hero.belongings.ring2 != null) {

			GLog.w(Game.getVar(R.string.Artifact_Limit));
			return false;

		} else {

			if (hero.belongings.ring1 == null) {
				hero.belongings.ring1 = this;
			} else {
				hero.belongings.ring2 = this;
			}

			detach(hero.belongings.backpack);

			activate(hero);

			cursedKnown = true;
			if (cursed) {
				equipCursed(hero);
				GLog.n(Utils.format(Game.getVar(R.string.Ring_Info2), this));
			}
			hero.spendAndNext(Artifact.TIME_TO_EQUIP);
			return true;
		}
	}

	@Override
	public boolean doUnequip(Hero hero, boolean collect, boolean single) {
		if (super.doUnequip(hero, collect, single)) {

			if (hero.belongings.ring1 == this) {
				hero.belongings.ring1 = null;
			} else {
				if (hero.belongings.ring2 == this) {
					hero.belongings.ring2 = null;
				} else { //WTF??
					throw new TrackedRuntimeException("trying unequip unequipped artifact");
				}
			}

			hero.remove(buff);
			buff = null;

			return true;
		} else {
			return false;
		}
	}

	@Override
	public ArrayList<String> actions(Hero hero) {
		ArrayList<String> actions = super.actions(hero);
		actions.add(isEquipped(hero) ? AC_UNEQUIP : AC_EQUIP);
		return actions;
	}

	@Override
	public boolean isEquipped(Hero hero) {
		return hero.belongings.ring1 == this || hero.belongings.ring2 == this;
	}

	public void activate(Char ch) {
		buff = buff();
		if (buff != null) {
			buff.attachTo(ch);
		}
	}

	@Override
	public boolean isUpgradable() {
		return false;
	}

	protected ArtifactBuff buff() {
		return null;
	}

	public class ArtifactBuff extends Buff {
		@Override
		public boolean dontPack() {
			return true;
		}
	}

	public String getText() {
		return null;
	}

	public int getColor() {
		return 0;
	}
}
