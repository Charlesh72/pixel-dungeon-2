/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.pixeldungeon.levels.traps;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.levels.TerrainFlags;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.windows.WndOptions;

public class TrapHelper {

	private static final String TXT_CHASM = Game.getVar(R.string.TrapWnd_Title);
	private static final String TXT_YES   = Game.getVar(R.string.Chasm_Yes);
	private static final String TXT_NO    = Game.getVar(R.string.Chasm_No);
	private static final String TXT_STEP  = Game.getVar(R.string.TrapWnd_Step);

	public static boolean stepConfirmed = false;

	public static boolean isVisibleTrap(int cellType){
		return TerrainFlags.is(cellType, TerrainFlags.TRAP) && !TerrainFlags.is(cellType, TerrainFlags.SECRET);
	}

	public static void heroTriggerTrap( final Hero hero ) {
		GameScene.show(
				new WndOptions( TXT_CHASM, TXT_STEP, TXT_YES, TXT_NO ) {
					@Override
					protected void onSelect( int index ) {
						if (index == 0) {
							stepConfirmed = true;
							hero.resume();
						}
					}
				}
		);
	}

	public static void heroPressed(){
		stepConfirmed = false;
	}
}
