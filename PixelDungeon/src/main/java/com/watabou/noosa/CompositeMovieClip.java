/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.noosa;

import com.watabou.glwrap.Texture;

import java.util.ArrayList;

/**
 * Created by mike on 29.03.2016.
 */
public class CompositeMovieClip extends MovieClip {

	private ArrayList<LayerDesc> mLayers;

	private class LayerDesc {
		String id;
		boolean enabled = true;
		Texture texture;

		LayerDesc(String _id, Texture _tex) {
			id = _id;
			texture = _tex;
		}
	}

	protected CompositeMovieClip() {
		super();
	}

	protected void clearLayers() {
		if (mLayers == null) {
			mLayers = new ArrayList<>();
		}

		mLayers.clear();
	}

	protected void addLayer(String id, Texture img) {
		if (mLayers == null) {
			mLayers = new ArrayList<>();
		}

		LayerDesc layerDesc = new LayerDesc(id, img);
		mLayers.add(layerDesc);
	}

	public void setLayerTexture(String id, Texture img) {
		if (mLayers != null) {
			for (LayerDesc layer : mLayers) {
				if (layer.id.equals(id)) {
					layer.texture = img;
				}
			}
		}
	}

	protected Texture getLayerTexture(String id) {
		if(mLayers!=null) {
			for (LayerDesc layer : mLayers) {
				if (layer.id.equals(id)) {
					return layer.texture;
				}
			}
		}
		return null;
	}

	public void setLayerState(String id, boolean state) {
		if (mLayers != null) {
			for (LayerDesc layer : mLayers) {
				if (layer.id.equals(id)) {
					layer.enabled = state;
				}
			}
		}
	}

	@Override
	public void draw() {
		super.draw();

		if (mLayers != null) {
			NoosaScript script = NoosaScript.get();

			for (LayerDesc layer : mLayers) {
				if (layer.enabled) {
					layer.texture.bind();
					script.drawQuad(verticesBuffer);
				}
			}
		}
	}
}
