/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.noosa;

import android.graphics.RectF;

import java.util.List;

/**
 * Created by mike on 29.03.2016.
 * This file is part of Pixel Dungeon 2.
 */
public class Animation {

	public float   delay;
	public RectF[] frames;
	public boolean looped;

	public Animation(int fps, boolean looped) {
		this.delay = 1f / fps;
		this.looped = looped;
	}

	public Animation frames(RectF... frames) {
		this.frames = frames;
		return this;
	}

	public Animation frames(int shift, TextureFilm film, int... frames) {
		this.frames = new RectF[frames.length];
		for (int i = 0; i < frames.length; i++) {
			this.frames[i] = film.get(frames[i] + shift);
		}
		return this;
	}

	public void frames(TextureFilm film, int... frames) {
		this.frames = new RectF[frames.length];
		for (int i = 0; i < frames.length; i++) {
			this.frames[i] = film.get(frames[i]);
		}
	}

	public Animation clone() {
		return new Animation(Math.round(1 / delay), looped).frames(frames);
	}

	public void frames(TextureFilm film, List<Integer> frames, int shift) {
		this.frames = new RectF[frames.size()];
		for (int i = 0; i < frames.size(); i++) {
			this.frames[i] = film.get(frames.get(i) + shift);
		}
	}
}
