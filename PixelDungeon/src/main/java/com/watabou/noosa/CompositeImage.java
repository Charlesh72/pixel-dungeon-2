/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.watabou.noosa;

import java.util.ArrayList;

public class CompositeImage extends Image {

	private ArrayList<Image> mLayers = new ArrayList<>();

	public CompositeImage() {
		super();
	}

	public CompositeImage(Object tx) {
		this();
		texture(tx);
	}

	public void addLayer(Image img) {
		mLayers.add(img);
	}

	@Override
	public void draw() {

		super.draw();

		NoosaScript script = NoosaScript.get();

		for (Image img : mLayers) {
			img.texture.bind();
			img.updateVerticesBuffer();
			script.drawQuad(img.verticesBuffer);
		}
	}
}
