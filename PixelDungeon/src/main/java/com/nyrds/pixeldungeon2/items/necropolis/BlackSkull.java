/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.necropolis;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.rings.Artifact;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.utils.Bundle;

import java.util.Collection;

public class BlackSkull extends Artifact {

	private static final int    ACTIVATED_IMAGE = 20;
	private static final int    BASIC_IMAGE     = 19;

	private static final int    RESSURRECTION_COST  = 10;
	private static final int    MAXIMUM_CHARGE  = 10;

	private static final String CHARGE_KEY      = "charge";
	private static final String ACTIVATED_KEY   = "activated";
	private static final String TXT_SKULL_ACTIVATED = Game.getVar(R.string.BlackSkull_Activated);
	private static final String TXT_SKULL_DEACTIVATED = Game.getVar(R.string.BlackSkull_Deactivated);
	private static final String TXT_SKULL_RESSURRECT = Game.getVar(R.string.BlackSkull_Ressurrect);

	private boolean activated = false;

	private int charge = 0;

	public BlackSkull() {
		imageFile = "items/artifacts.png";
		identify();
		image = BASIC_IMAGE;
	}

	public void mobDied(Mob mob, Hero hero) {
		Collection<Mob> pets = hero.getPets();

		if (pets.contains(mob)){
			return;
		}

		if (mob.canBePet()) {
			if (activated) {
				mob.ressurrect(hero);
				GLog.w( TXT_SKULL_RESSURRECT );
				charge = charge - RESSURRECTION_COST;
				if (charge <= 0) {
					GLog.w( TXT_SKULL_DEACTIVATED );
					activated = false;
				}
			} else {
				charge++;
				if (charge >= MAXIMUM_CHARGE) {
					GLog.w( TXT_SKULL_ACTIVATED );
					activated = true;
				}
			}
		}
	}

	@Override
	public int image() {
		if (activated) {
			return ACTIVATED_IMAGE;
		} else {
			return BASIC_IMAGE;
		}
	}

	@Override
	public String info() {
		if (activated) {
			return Game.getVar(R.string.BlackSkull_Info_Awakened);
		} else {
			return Game.getVar(R.string.BlackSkull_Info);
		}
	}

	@Override
	public String name() {
		if (activated) {
			return Game.getVar(R.string.BlackSkull_Name_Awakened);
		} else {
			return Game.getVar(R.string.BlackSkull_Name);
		}
	}

	@Override
	public void storeInBundle(Bundle bundle) {
		super.storeInBundle(bundle);

		bundle.put(CHARGE_KEY, charge);
		bundle.put(ACTIVATED_KEY, activated);
	}

	@Override
	public void restoreFromBundle(Bundle bundle) {
		super.restoreFromBundle(bundle);

		charge = bundle.getInt(CHARGE_KEY);
		activated = bundle.getBoolean(ACTIVATED_KEY);
	}
}
