/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package com.nyrds.pixeldungeon2.items.common.rings;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.items.rings.Artifact;
import com.watabou.pixeldungeon.sprites.ItemSprite;
import com.watabou.pixeldungeon.ui.BuffIndicator;

public class RingOfFrost extends Artifact {

	public RingOfFrost() {
		imageFile = "items/rings.png";
		image = 13;
		identify();
	}

	@Override
	public ItemSprite.Glowing glowing() {
		return new ItemSprite.Glowing( 0x00FFFF );
	}

	@Override
	protected ArtifactBuff buff( ) {
		return new FrostAura();
	}

	@Override
	public boolean isUpgradable() {
		return true;
	}

	public class FrostAura extends ArtifactBuff {
		@Override
		public int icon() {
			return BuffIndicator.FROSTAURA;
		}

		@Override
		public String toString() {
			return Game.getVar(R.string.FrostAura_Buff);
		}
	}
}
