/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.artifacts;

import com.nyrds.Packable;
import com.nyrds.pixeldungeon2.mechanics.spells.Spell;
import com.nyrds.pixeldungeon2.mechanics.spells.SpellFactory;
import com.nyrds.pixeldungeon2.mechanics.spells.SpellHelper;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.rings.Artifact;
import com.watabou.pixeldungeon.utils.Utils;
import com.watabou.utils.Random;

import java.util.ArrayList;

public class SpellBook extends Artifact {

	public static final String AC_USE = Game.getVar(R.string.SpiderCharm_Use);
	public static final String IDENTIFIED = Game.getVar(R.string.SpellBook_Info_Identified);

	@Packable
	private String spell;

	public SpellBook() {
		imageFile = "items/books.png";
		image = 3;
	}

	public void spell(String spellName){
		spell = spellName;
	}

	public Spell spell(){
		if(spell == null || spell.equals("")){
			ArrayList<String> spells = SpellFactory.getSpellsByAffinity(SpellHelper.AFFINITY_COMMON);
			spell(Random.element(spells));
		}
		return SpellFactory.getSpellByName(spell);
	}

	@Override
	public ArrayList<String> actions(Hero hero ) {
		ArrayList<String> actions = super.actions( hero );
		if (isEquipped(hero)){
			actions.add( AC_USE );
		}
		return actions;
	}

	@Override
	public void execute(final Hero ch, String action) {
		setCurUser(ch);
		if (action.equals(AC_USE)) {
			spell().cast(ch);
		} else {
			super.execute(ch, action);
		}
	}

	@Override
	public String desc(){
		if(this.isIdentified()){
			return  Utils.format(IDENTIFIED, spell().name(), spell().desc());
		}
		return super.desc();
	}

	@Override
	public Item burn(int cell){
		return null;
	}
}
