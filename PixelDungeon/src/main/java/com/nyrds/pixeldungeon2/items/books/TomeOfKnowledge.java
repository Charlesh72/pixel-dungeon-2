/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.books;

import com.nyrds.pixeldungeon2.mechanics.spells.SpellHelper;
import com.watabou.noosa.audio.Sample;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.effects.Speck;
import com.watabou.pixeldungeon.effects.SpellSprite;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.sprites.CharSprite;

public class TomeOfKnowledge extends Book {

	{
		image = getImageIndex();
	}

	private int getImageIndex() {
		String affinity = Dungeon.hero.heroClass.getMagicAffinity();
		if (affinity.equals(SpellHelper.AFFINITY_ELEMENTAL)) {
			return 1;
		}
		if (affinity.equals(SpellHelper.AFFINITY_NECROMANCY)) {
			return 2;
		}
		return 0;
	}

	@Override
	protected void doRead(Hero hero) {
		SpellSprite.show( getCurUser(), SpellSprite.MASTERY );
		getCurUser().getSprite().emitter().burst( Speck.factory( Speck.MAGIC ), 8 );
		hero.getSprite().showStatus( CharSprite.BLUE, "+ 1");
		Sample.INSTANCE.play( Assets.SND_READ );

		getCurUser().spendAndNext( TIME_TO_READ );
		getCurUser().busy();
		hero.magicLvlUp();
	}

	@Override
	public int price() {
		return 100;
	}

	@Override
	public Item burn(int cell){
		return null;
	}
}
