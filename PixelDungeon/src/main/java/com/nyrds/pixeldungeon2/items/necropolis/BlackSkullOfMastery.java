/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.necropolis;

import com.nyrds.pixeldungeon2.items.common.MasteryItem;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.hero.HeroSubClass;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.pixeldungeon.windows.WndChooseWay;

import java.util.ArrayList;

public class BlackSkullOfMastery extends MasteryItem {

	public static final float TIME_TO_READ = 10;

	public static final String AC_NECROMANCY = Game.getVar(R.string.Necromancer_ACSpecial);

	{
		stackable = false;
		imageFile = "items/artifacts.png";
		identify();
		image = 19;
	}
	
	@Override
	public ArrayList<String> actions( Hero hero ) {
		ArrayList<String> actions = super.actions( hero );		
		actions.add( AC_NECROMANCY );
		
		return actions;
	}
	
	@Override
	public void execute( Hero hero, String action ) {
		if (action.equals( AC_NECROMANCY )) {

			setCurUser(hero);
			
			HeroSubClass way;

			switch (hero.heroClass) {
			default:
				GLog.w("Error: How did you get this item?! You're not supposed to be able to obtain it!!");
				return;
			case NECROMANCER:
				way = HeroSubClass.LICH;
				hero.setMaxSoulPoints(hero.getSoulPointsMax() * 2);
				break;
			}
			GameScene.show( new WndChooseWay( this, way ) );
		} else {
			super.execute( hero, action );
		}
	}
}
