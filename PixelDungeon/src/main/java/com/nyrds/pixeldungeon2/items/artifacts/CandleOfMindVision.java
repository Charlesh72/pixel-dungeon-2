/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.artifacts;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.buffs.Buff;
import com.watabou.pixeldungeon.actors.buffs.CandleOfMindVisionBuff;
import com.watabou.pixeldungeon.actors.buffs.MindVision;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.items.rings.Artifact;
import com.watabou.utils.Bundle;

public class CandleOfMindVision extends Artifact implements IActingItem {

	private static final String CHARGES   = "charges";
	private static final String ACTIVATED = "activated";

	private static final int MAX_CHARGES = 50;

	private float   charges;
	private boolean activated;


	public CandleOfMindVision() {
		imageFile = "items/candle.png";
		charges   = MAX_CHARGES;
		activated = false;
	}

	@Override
	public boolean isIdentified() {
		return true;
	}

	@Override
	public void storeInBundle(Bundle bundle) {
		super.storeInBundle(bundle);

		bundle.put(CHARGES, charges);
		bundle.put(ACTIVATED, activated);
	}

	@Override
	public void restoreFromBundle(Bundle bundle) {
		super.restoreFromBundle(bundle);

		charges   = bundle.getFloat(CHARGES);
		activated = bundle.getBoolean(ACTIVATED);
	}

	@Override
	public boolean doEquip(Hero hero) {
		boolean ret = super.doEquip(hero);
		
		if(ret) {
			if (!activated) {
				activated = true;
			}

			if (charges > 0) {
				Buff.affect(hero, CandleOfMindVisionBuff.class, charges);
				MindVision.reportMindVisionEffect();
			}
		}
		return ret;
	}

	@Override
	public boolean doUnequip(Hero hero, boolean collect) {
		if(charges > 0) {
			Buff.detach(hero, CandleOfMindVisionBuff.class);
		}
		return super.doUnequip(hero, collect);
	}

	@Override
	public int image() {
		if (!activated) {
			return 0;
		} else {
			if (charges > MAX_CHARGES / 4 * 3) {
				return 1;
			}
			if (charges > MAX_CHARGES / 4) {
				return 2;
			}
			if (charges > 0) {
				return 3;
			}
			return 4;
		}
	}

	@Override
	public String desc() {
		if(!activated) {
			return Game.getVar(R.string.CandleOfMindVision_Info);
		} else {
			if(charges>0) {
				return Game.getVar(R.string.CandleOfMindVision_Info_Lighted);
			} else {
				return Game.getVar(R.string.CandleOfMindVision_Info_Exhausted);
			}
		}
	}

	@Override
	public void spend(Char hero, float time) {
		if (activated) {
			if (time > 0) {
				charges -= time;
			}
		}
	}

}
