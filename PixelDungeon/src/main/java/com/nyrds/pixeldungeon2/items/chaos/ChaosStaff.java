/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.chaos;

import com.nyrds.android.util.TrackedRuntimeException;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.mobs.Boss;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.items.potions.PotionOfHealing;
import com.watabou.pixeldungeon.items.wands.Wand;
import com.watabou.pixeldungeon.items.wands.WandOfTeleportation;
import com.watabou.utils.Bundle;
import com.watabou.utils.Random;

public class ChaosStaff extends Wand implements IChaosItem {

    private int charge = 0;

    public ChaosStaff() {
        imageFile = "items/chaosStaff.png";
        image = 0;
    }

    @Override
    protected void updateLevel() {
        super.updateLevel();
        selectImage();
    }

    @Override
    public void ownerTakesDamage(int damage) {
        charge++;
    }

    @Override
    public void ownerDoesDamage(Char ch, int damage) {
    }

    private void selectImage() {
        image = Math.max(0, Math.min(level() / 3, 4));
    }

    @Override
    public void storeInBundle(Bundle bundle) {
        super.storeInBundle(bundle);

        bundle.put(ChaosCommon.CHARGE_KEY, charge);
    }

    @Override
    public void restoreFromBundle(Bundle bundle) {
        super.restoreFromBundle(bundle);

        charge = bundle.getInt(ChaosCommon.CHARGE_KEY);
    }

    @Override
    public int getCharge() {
        return charge;
    }

    @Override
    protected void onZap(int cell) {

        ChaosCommon.doChaosMark(cell, 10 + level() * 10 + charge);
        charge = 0;

        if (Math.random() < 0.1f) {
            Char ch = Actor.findChar(cell);
            if (ch instanceof Mob) {
                Mob mob = (Mob) ch;

                if ((mob instanceof Boss) || (mob instanceof NPC)) {
                    return;
                }

                switch (Random.Int(0, 4)) {
                    case 0:
                        mob.die(getCurUser());
                        break;
                    case 1:
                        Mob.makePet(mob, getCurUser());
                        break;

                    case 2:
                        int nextCell = Dungeon.level.getEmptyCellNextTo(cell);

                        if (Dungeon.level.cellValid(nextCell)) {
                            try {
                                Mob newMob = mob.getClass().newInstance();
                                Dungeon.level.spawnMob(newMob);
                            } catch (Exception e) {
                                throw new TrackedRuntimeException(e);
                            }
                        }
                        break;
                    case 3:
                        WandOfTeleportation.teleport(mob);
                        break;
                    case 4:
                        PotionOfHealing.heal(ch, 1);
                        break;
                }
            }
        }
    }

    @Override
    public String name() {
        return Game.getVar(R.string.ChaosStaff_Name);
    }

    @Override
    public String info() {
        return Game.getVar(R.string.ChaosStaff_Info);
    }
}
