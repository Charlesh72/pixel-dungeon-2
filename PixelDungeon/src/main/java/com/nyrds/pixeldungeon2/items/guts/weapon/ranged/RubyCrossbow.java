/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.guts.weapon.ranged;

import com.watabou.pixeldungeon.items.weapon.melee.Bow;

public class RubyCrossbow extends Bow {

	public RubyCrossbow() {
		super( 6, 1.1f, 1.6f );
		imageFile = "items/ranged.png";
		image = 5;
	}

	@Override
	public double acuFactor() {
		return 1;
	}

	@Override
	public double dmgFactor() {
		return 1 + level() * 1.1;
	}

	public double dlyFactor() {
		return 1 + level() * 0.2;
	}
}
