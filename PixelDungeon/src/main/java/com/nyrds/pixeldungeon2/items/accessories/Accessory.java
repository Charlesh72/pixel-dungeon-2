/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.accessories;

import com.nyrds.android.util.TrackedRuntimeException;
import com.nyrds.pixeldungeon2.ml.R;
import com.nyrds.pixeldungeon2.support.Iap;
import com.watabou.noosa.Game;
import com.watabou.noosa.Image;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.PixelDungeon;
import com.watabou.pixeldungeon.Preferences;
import com.watabou.pixeldungeon.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DeadDie on 26.05.2016
 */
public class Accessory {

	protected boolean coverHair;
	protected int image = 0;
	protected static String imageFile = "items/accessories.png";

	protected String name = getClassParam("Name", Game.getVar(R.string.Item_Name), false);
	protected String info = getClassParam("Info", Game.getVar(R.string.Item_Info), false);

	static final private Map<String,Class<? extends Accessory>> allAccessoriesList = new HashMap<>();

	private static void registerAccessory(Class<? extends Accessory> Clazz) {
		allAccessoriesList.put(Clazz.getSimpleName(), Clazz);
	}

	static {
		registerAccessory(Fez.class);
		registerAccessory(Pumpkin.class);
		registerAccessory(Capotain.class);
		registerAccessory(Bowknot.class);
		registerAccessory(Nightcap.class);
		registerAccessory(RabbitEars.class);
		registerAccessory(WizardHat.class);
		registerAccessory(Shades.class);
		registerAccessory(NekoEars.class);
		registerAccessory(PirateSet.class);
		registerAccessory(ZombieMask.class);
		registerAccessory(VampireSkull.class);
		registerAccessory(Ushanka.class);
		registerAccessory(SantaHat.class);
		registerAccessory(Rudolph.class);
		registerAccessory(GnollCostume.class);
		registerAccessory(ChaosHelmet.class);
	}

	public static List<String> getAccessoriesList() {
		return new ArrayList<>(allAccessoriesList.keySet());
	}

	public String getLayerFile() {
		return "hero/accessories/"+ getClass().getSimpleName() + ".png";
	}

    Accessory (){
		//imageFile = "items/accessories.png";
        coverHair = false;
    }

	public boolean isCoveringHair() {
		return coverHair;
	}

	public static Accessory getByName(String name) {
		try {
			return allAccessoriesList.get(name).newInstance();
		} catch (Exception e) {
			throw new TrackedRuntimeException(e);
		}
	}

	protected String getClassParam(String paramName, String defaultValue, boolean warnIfAbsent) {
		return Utils.getClassParam(this.getClass().getSimpleName(), paramName, defaultValue, warnIfAbsent);
	}

	public Image getImage() {
		return new Image(imageFile, image*28, 0, 28,28);
	}

	public static Image getSlotImage() {
		return new Image(imageFile, 0, 0, 28,28);
	}

	public String desc() {
		return info;
	}
	public String name() {
		return name;
	}

	private String prefProperty() {
		return "Accessory"+getClass().getSimpleName();
	}

	static public void check() {
		for(String item:allAccessoriesList.keySet()) {
            if(Iap.checkPurchase(item)) {
				getByName(item).ownIt(true);
			}   else {
				getByName(item).ownIt(false);
			}
		}
	}

	public boolean haveIt() {
		if (PixelDungeon.donated() == 4){
			return true;
		}
		return Preferences.INSTANCE.getString(prefProperty(),"").equals(getClass().getSimpleName());
	}

	public void ownIt(boolean reallyOwn) {
		if(reallyOwn) {
			Preferences.INSTANCE.put(prefProperty(), getClass().getSimpleName());
		} else {
			Preferences.INSTANCE.put(prefProperty(), "");
		}
	}

	public void equip (){
		if(!haveIt()) {
			return;
		}

		Preferences.INSTANCE.put(Accessory.class.getSimpleName(), getClass().getSimpleName());
	}

	public static void unequip (){
		Preferences.INSTANCE.put(Accessory.class.getSimpleName(), "");
		Dungeon.hero.updateLook();
	}

	static public Accessory equipped() {
		String itemName = Preferences.INSTANCE.getString(Accessory.class.getSimpleName(),"");
		if(!itemName.equals("")) {
			return getByName(itemName);
		}

		return null;
	}
}
