/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.items.chaos;

import com.watabou.noosa.audio.Sample;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.actors.blobs.Blob;
import com.watabou.pixeldungeon.actors.blobs.ConfusionGas;
import com.watabou.pixeldungeon.actors.blobs.LiquidFlame;
import com.watabou.pixeldungeon.actors.blobs.ParalyticGas;
import com.watabou.pixeldungeon.actors.blobs.Regrowth;
import com.watabou.pixeldungeon.actors.blobs.ToxicGas;
import com.watabou.pixeldungeon.effects.CellEmitter;
import com.watabou.pixeldungeon.effects.particles.PurpleParticle;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.utils.Random;

public class ChaosCommon {
	public static final String CHARGE_KEY = "charge";
	
	@SuppressWarnings("rawtypes")
	private static Class[] blobs = {
		ConfusionGas.class,
		LiquidFlame.class,
		ParalyticGas.class,
		Regrowth.class,
		ToxicGas.class,
	};
	
	@SuppressWarnings("unchecked")
	public static void doChaosMark(int cell, int charge) {
		if(charge > 0) {
			CellEmitter.center( cell ).burst( PurpleParticle.BURST, Random.IntRange( 10, 20 ) );
			Sample.INSTANCE.play( Assets.SND_CRYSTAL );
			GameScene.add(Blob.seed(cell, charge, Random.element(blobs)));
			GameScene.add(Blob.seed(cell, charge, Random.element(blobs)));
		}
	}
}
