/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.utils;

import com.watabou.pixeldungeon.levels.Level;
import com.watabou.utils.Bundlable;
import com.watabou.utils.Bundle;

public class Position implements Bundlable {

	public int cellId = -1;
	public String levelId;

	public int x = -1;
	public int y = -1;

	@Deprecated
	private static final String LEVEL_DEPTH = "levelDepth";
	@Deprecated
	private static final String LEVEL_KIND  = "levelKind";

	private static final String LEVEL_ID = "levelId";
	private static final String CELL_ID  = "cellId";

	private static final String X = "x";
	private static final String Y = "y";

	public Position(String _levelId, int _cellId) {
		levelId = _levelId;
		cellId = _cellId;
	}

	public Position(String _levelId, int _x, int _y) {
		levelId = _levelId;
		x = _x;
		y = _y;
	}

	public Position(Position pos) {
		levelId = pos.levelId;
		cellId = pos.cellId;
		x = pos.x;
		y = pos.y;
	}

	public Position() {
	}

	@Override
	public void restoreFromBundle(Bundle bundle) {
		int levelDepth = bundle.optInt(LEVEL_DEPTH, 1);
		String levelKind = bundle.optString(LEVEL_KIND, "SewerLevel");

		cellId = bundle.optInt(CELL_ID, -1);

		x = bundle.optInt(X, -1);
		y = bundle.optInt(Y, -1);

		levelId = bundle.optString(LEVEL_ID, DungeonGenerator.guessLevelId(levelKind, levelDepth));
	}

	@Override
	public void storeInBundle(Bundle bundle) {
		bundle.put(LEVEL_ID, levelId);

		if (cellId >= 0) {
			bundle.put(CELL_ID, cellId);
		}

		if (x >= 0 && y >= 0) {
			bundle.put(X, x);
			bundle.put(Y, y);
		}
	}

	public boolean dontPack() {
		return false;
	}

	public void computeCell(Level level) {
		if(x>=0&&y>=0) {
			cellId = x + y*level.getWidth();
		}
	}
}
