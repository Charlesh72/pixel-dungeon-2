/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.levels.objects;

import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.utils.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mike on 01.07.2016.
 */
public class ConcreteBlock extends LevelObject {

	int requiredStr = 10;

	public ConcreteBlock() {
		this(-1);
	}

	public ConcreteBlock(int pos) {
		super(pos);
		imageIndex = 8;
	}

	@Override
	void setupFromJson(Level level, JSONObject obj) throws JSONException {
		requiredStr = obj.optInt("str", 10);
	}

	@Override
	public boolean stepOn(Char hero) {
		return false;
	}

	@Override
	public boolean pushable(Char hero) {
		return true;
	}

	@Override
	public boolean push(Char hero) {
		if(hero instanceof Hero) {
			return ((Hero)hero).effectiveSTR() >= requiredStr && super.push(hero);
		}
		return super.push(hero);
	}

	@Override
	public void restoreFromBundle(Bundle bundle) {
		super.restoreFromBundle(bundle);
		requiredStr = bundle.getInt("str");
	}

	@Override
	public void storeInBundle(Bundle bundle) {
		super.storeInBundle(bundle);
		bundle.put("str",requiredStr);
	}

	@Override
	public String desc() {
		return String.format(Game.getVar(R.string.ConcreteBlock_Description), requiredStr);
	}

	@Override
	public String name() {
		return Game.getVar(R.string.ConcreteBlock_Name);
	}

	@Override
	public boolean affectLevelObjects() {
		return true;
	}

	@Override
	public boolean nonPassable() {
		return true;
	}
}
