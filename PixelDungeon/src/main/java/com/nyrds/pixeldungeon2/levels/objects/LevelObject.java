/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.levels.objects;

import com.nyrds.Packable;
import com.nyrds.android.util.Util;
import com.nyrds.pixeldungeon2.levels.objects.sprites.LevelObjectSprite;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.utils.Bundlable;
import com.watabou.utils.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class LevelObject implements Bundlable, Presser {

    @Packable
    private int pos = -1;

    @Packable
    protected int layer = 0;

    @Packable(defaultValue = "levelObjects/objects.png")
    protected String textureFile = "levelObjects/objects.png";

    @Packable(defaultValue = "0")
    protected int imageIndex = 0;

    public LevelObjectSprite sprite;

    public LevelObject(int pos) {
        this.pos = pos;
    }

    public int image() {
        return imageIndex;
    }

    void setupFromJson(Level level, JSONObject obj) throws JSONException {
        textureFile = obj.optString("textureFile", textureFile);
        imageIndex = obj.optInt("imageIndex", imageIndex);
    }

    public boolean interact(Char hero) {
        return true;
    }

    public boolean stepOn(Char hero) {
        return true;
    }

    public boolean nonPassable() {
        return false;
    }

    protected void remove() {
        Dungeon.level.remove(this);
        sprite.kill();
    }

    public void burn() {
    }

    public void open() {

    }

    public void freeze() {
    }

    public void poison() {
    }

    public void bump(Presser presser) {
    }

    public void discover() {
    }

    public boolean secret() {
        return false;
    }

    @Override
    public void restoreFromBundle(Bundle bundle) {
    }

    @Override
    public void storeInBundle(Bundle bundle) {
    }

    public boolean dontPack() {
        return false;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        if (sprite != null) {
            sprite.move(this.pos, pos);
            Dungeon.level.levelObjectMoved(this);
        }

        this.pos = pos;
    }

    public abstract String desc();

    public abstract String name();

    public String texture() {
        return textureFile;
    }

    public boolean pushable(Char hero) {
        return false;
    }

    public boolean push(Char hero) {
        Level level = Dungeon.level;

        int hx = level.cellX(hero.getPos());
        int hy = level.cellY(hero.getPos());

        int x = level.cellX(getPos());
        int y = level.cellY(getPos());

        int dx = x - hx;
        int dy = y - hy;

        if (dx * dy != 0) {
            return false;
        }

        int nextCell = level.cell(x + Util.signum(dx), y + Util.signum(dy));

        if (!level.cellValid(nextCell)) {
            return false;
        }

        if (level.solid[nextCell] || level.getLevelObject(nextCell, layer) != null) {
            return false;
        } else {

            setPos(nextCell);
            level.levelObjectMoved(this);
            level.press(nextCell, this);

        }

        return true;
    }

    public void fall() {
        if (sprite != null) {
            sprite.fall();
            Dungeon.level.remove(this);
        }
    }

    @Override
    public boolean affectLevelObjects() {
        return false;
    }

    public int getSpriteXS() {
        return 16;
    }

    public int getSpriteYS() {
        return 16;
    }

    public int getLayer() {
        return layer;
    }
}