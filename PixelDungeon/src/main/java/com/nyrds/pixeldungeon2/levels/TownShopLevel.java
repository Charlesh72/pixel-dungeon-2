/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.levels;

import com.nyrds.pixeldungeon2.items.books.TomeOfKnowledge;
import com.nyrds.pixeldungeon2.mobs.npc.TownShopkeeper;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.Torch;
import com.watabou.pixeldungeon.items.armor.LeatherArmor;
import com.watabou.pixeldungeon.items.bags.Keyring;
import com.watabou.pixeldungeon.items.bags.PotionBelt;
import com.watabou.pixeldungeon.items.bags.Quiver;
import com.watabou.pixeldungeon.items.bags.ScrollHolder;
import com.watabou.pixeldungeon.items.bags.SeedPouch;
import com.watabou.pixeldungeon.items.bags.WandHolster;
import com.watabou.pixeldungeon.items.food.OverpricedRation;
import com.watabou.pixeldungeon.items.weapon.melee.Dagger;
import com.watabou.pixeldungeon.items.weapon.melee.Knuckles;
import com.watabou.pixeldungeon.items.weapon.melee.Quarterstaff;
import com.watabou.pixeldungeon.items.weapon.melee.Sword;
import com.watabou.pixeldungeon.items.weapon.missiles.CommonArrow;
import com.watabou.pixeldungeon.items.weapon.missiles.Dart;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.pixeldungeon.levels.painters.Painter;

import java.util.ArrayList;
import java.util.Arrays;

public class TownShopLevel extends Level {

	private static final int SIZE = 12;

	{
		color1 = 0x801500;
		color2 = 0xa68521;
	}

	@Override
	public String tilesTex() {
		return Assets.TILES_GENERIC_TOWN_INTERIOR;
	}

	@Override
	public String waterTex() {
		return Assets.WATER_SEWERS;
	}

	@Override
	protected boolean build() {
		Arrays.fill( map, Terrain.WALL );
		Painter.fill( this, 2, 2, SIZE-2, SIZE-2, Terrain.EMPTY_SP );
		Painter.fill( this, SIZE/2, SIZE/2, 2, 2, Terrain.EMPTY );

		entrance = SIZE * getWidth() + SIZE / 2 + 1;
		map[entrance] = Terrain.ENTRANCE;


		setFeeling(Feeling.NONE);

		makeShop();
		return true;
	}

	private void makeShop(){
		ArrayList<Item> items = new ArrayList<>();

		items.add( new LeatherArmor().identify() );
		items.add( new Dagger().identify() );
		items.add( new Knuckles().identify() );
		items.add( new Sword().identify() );
		items.add( new Quarterstaff().identify() );
		items.add( new TomeOfKnowledge().identify() );

		for (int i = 0; i <3; i++){
			items.add( new OverpricedRation() );
			items.add( new Dart(5).identify() );
			items.add( new CommonArrow(25) );
			items.add( new Torch().identify() );
		}

		items.add( new Keyring());
		items.add( new ScrollHolder());
		items.add( new PotionBelt());
		items.add( new SeedPouch());
		items.add( new Quiver());
		items.add( new WandHolster());

		Item[] range = items.toArray(new Item[items.size()]);

		for (Item item : range) {
			itemForSell(item);
		}
	}

	public void itemForSell(Item item) {
		int cell;
		int ctr = 0;
		do {
			ctr++;
			if(ctr>25) {
				return;
			}

			cell = getRandomTerrainCell(Terrain.EMPTY_SP);

		} while (getHeap(cell) != null);

		drop(item, cell).type = Heap.Type.FOR_SALE;
	}

	@Override
	protected void decorate() {
	}

	@Override
	protected void createMobs() {
		Mob shopkeeper =  new TownShopkeeper();
		shopkeeper.setPos(this.getRandomTerrainCell(Terrain.EMPTY));
		this.mobs.add( shopkeeper );
	}

	@Override
	protected void createItems() {
	}

	@Override
	public int randomRespawnCell() {
		return -1;
	}

	@Override
	public String tileName( int tile ) {
		switch (tile) {
		default:
			return super.tileName( tile );
		}
	}

	@Override
	public String tileDesc(int tile) {
		switch (tile) {
		default:
			return super.tileDesc(tile);
		}
	}
}
