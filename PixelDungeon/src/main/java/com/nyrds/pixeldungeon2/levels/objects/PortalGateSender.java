/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.levels.objects;

import com.nyrds.pixeldungeon2.utils.Position;
import com.nyrds.pixeldungeon2.windows.WndPortal;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.items.Amulet;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.utils.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

public class PortalGateSender extends PortalGate {

	static final String TARGET = "target";

	protected Position target;

	@Override
	public boolean interact(Char chr) {

		if(!(chr instanceof Char)) {
			return false;
		}

		Hero hero = (Hero)chr;

		if(!used && hero.belongings.getItem(Amulet.class) == null){
			if(!animationRunning){
				if (!activated){
					playStartUpAnim();
				} else {
					GameScene.show(new WndPortal(this, hero, target));
				}
			}
		} else{
			GLog.w( TXT_USED );
		}
		return false;
	}

	@Override
	void setupFromJson(Level level, JSONObject obj) throws JSONException {
		super.setupFromJson(level, obj);

		if(obj.has("target")){
			JSONObject portalDesc = obj.getJSONObject("target");
			String levelId = portalDesc.optString("levelId" ,"1");
			target = new Position(levelId, portalDesc.optInt("x" ,1), portalDesc.optInt("y" ,1));
		}
	}

	@Override
	public void restoreFromBundle( Bundle bundle ) {
		super.restoreFromBundle(bundle);
		target = (Position) bundle.get(TARGET);
	}

	@Override
	public void storeInBundle( Bundle bundle ) {
		super.storeInBundle(bundle);
		bundle.put(TARGET, target);
	}
}
