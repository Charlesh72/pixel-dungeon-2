/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.levels.objects;

import com.nyrds.android.util.ModdingMode;
import com.nyrds.android.util.Util;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.noosa.audio.Sample;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.blobs.Blob;
import com.watabou.pixeldungeon.actors.blobs.LiquidFlame;
import com.watabou.pixeldungeon.actors.buffs.Buff;
import com.watabou.pixeldungeon.actors.buffs.Burning;
import com.watabou.pixeldungeon.actors.buffs.Paralysis;
import com.watabou.pixeldungeon.actors.buffs.Poison;
import com.watabou.pixeldungeon.actors.buffs.Roots;
import com.watabou.pixeldungeon.actors.buffs.Slow;
import com.watabou.pixeldungeon.items.Dewdrop;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.sprites.ItemSprite;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.utils.Callback;
import com.watabou.utils.Random;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mike on 01.07.2016.
 */
public class Barrel extends LevelObject {

	private boolean burned = false;
	private boolean opened = false;
	private int type = 0;

	private static final int NORMAL = 0;
	private static final int POISION = 1;
	private static final int DEW = 2;
	private static final int PARALYZE = 3;

	public Barrel() {
		this(-1);
	}

	public Barrel(int pos) {
		super(pos);
		textureFile = "levelObjects/barrels.png";
		type = Random.Int(4);
		if (type != DEW)
		    type = Random.Int(4);
	}

	@Override
	void setupFromJson(Level level, JSONObject obj) throws JSONException {
	}

	@Override
	public boolean pushable(Char hero) {
		return true;
	}

	@Override
	public void burn() {

		if (burned) {
			return;
		}

		burned = true;

		sprite.playAnim(10, false, new Callback() {
			@Override
			public void call() {
				remove();

			}
		}, image() + 0, image() + 1, image() + 2, image() + 3, image() + 4);


		Sample.INSTANCE.play(Assets.SND_EXPLOSION);

		LiquidFlame fire = Blob.seed(getPos(), 10, LiquidFlame.class);
		GameScene.add(fire);
	}

	@Override
	public void bump(Presser presser) {
		burn();
	}

	@Override
	public String desc() {

		if (ModdingMode.isHalloweenEvent()) {
			return Game.getVar(R.string.Barrel_Pumpkin_Desc);
		} else {
			return Game.getVar(R.string.Barrel_Desc);
		}

	}

	@Override
	public String name() {
		if (ModdingMode.isHalloweenEvent()) {
			return Game.getVar(R.string.Barrel_Pumpkin_Name);
		} else {
			return Game.getVar(R.string.Barrel_Name);
		}
	}

	@Override
	public int image() {
		if (ModdingMode.isHalloweenEvent()) {
			return 0;
		} else {
			return 8 + type * 8;
		}
	}

	@Override
	public boolean affectLevelObjects() {
		return true;
	}

	@Override
	public boolean nonPassable() {
		return true;
	}

	@Override
	public boolean push(Char hero) {
		Level level = Dungeon.level;

		int hx = level.cellX(hero.getPos());
		int hy = level.cellY(hero.getPos());

		int pos = getPos();
		int x = level.cellX(getPos());
		int y = level.cellY(getPos());

		int dx = x - hx;
		int dy = y - hy;

		if (dx * dy != 0) {
			return false;
		}

		int nextCell = level.cell(x + Util.signum(dx), y + Util.signum(dy));

		if (!level.cellValid(nextCell)) {
			return false;
		}

		if (level.solid[nextCell] || level.getLevelObject(nextCell, layer) != null) {
			return false;
		} else {

			setPos(nextCell);
			level.levelObjectMoved(this);
			level.press(nextCell, this);

		}
		open(hero, pos);
		return true;
	}

	public void open(Char hero, int pos) {
	    if (opened)
	        return;
	    opened = true;

        switch (type) {
            case NORMAL:
                break;
            case POISION:
                GLog.w(Game.getVar(R.string.MysteryMeat_Info3));
                Buff.affect( hero, Poison.class ).set( Poison.durationFactor( hero ) * hero.ht() / 5 );
                break;
            case DEW:
                if(!new Dewdrop().doPickUp(Dungeon.hero)) {
                    ItemSprite is = Dungeon.level.drop( new Dewdrop(), pos ).sprite;
                    if(is != null) {
                        is.drop();
                    }
                }
                break;
            case PARALYZE:
                GLog.w(Game.getVar(R.string.MysteryMeat_Info2));
                Buff.prolong( hero, Roots.class, Paralysis.duration( hero ) );
                break;
        }
    }
}
