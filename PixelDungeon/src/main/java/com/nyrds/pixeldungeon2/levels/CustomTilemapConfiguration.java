/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.levels;

import com.nyrds.android.util.JsonHelper;
import com.watabou.pixeldungeon.levels.Terrain;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mike on 26.12.2016.
 * This file is part of Pixel Dungeon 2.
 */

public class CustomTilemapConfiguration {

	Map<Terrain,ArrayList<tileProperties>> tilemapConfiguration = new HashMap<>();

	static CustomTilemapConfiguration readConfig(String filename) {

		JSONObject config = JsonHelper.readJsonFromAsset(filename);

		CustomTilemapConfiguration ret = new CustomTilemapConfiguration();
		//ret.tilemapConfiguration.put(Terrain.)
		return ret;
	}

	public class tileProperties {
		public int terrainType;
		public int moreProps;
	}
}
