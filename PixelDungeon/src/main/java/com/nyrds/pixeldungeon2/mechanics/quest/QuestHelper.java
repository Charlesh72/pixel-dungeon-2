/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mechanics.quest;

import com.nyrds.android.util.JsonHelper;
import com.nyrds.android.util.TrackedRuntimeException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;

/**
 * Created by DeadDie on 14.05.2017
 */
public class QuestHelper {
    static private JSONObject initQuest = JsonHelper.readJsonFromAsset("questDesc/quests.json");
    static private JSONObject initQuestList = JsonHelper.readJsonFromAsset("questDesc/questList.json");

    public static HashSet<Quest> questMap;

    public void initQuest(String questName) {
        if (initQuest.has(questName)) {
            try {
                JSONObject questDesc = initQuest.getJSONObject(questName);

                int quantity = Math.max(questDesc.optInt("quantity",1),1);

                if (questDesc.has("type")) {
                    createQuest(questDesc.getString("type"));
                }

                if (questDesc.has("target")) {

                }

                if (questDesc.has("startMessage")) {

                }

                if (questDesc.has("reminderMessage")) {

                }

                if (questDesc.has("turnInMessage")) {

                }
                if (questDesc.has("aftermathMessage")) {

                }

            } catch (JSONException e) {
                throw new TrackedRuntimeException(e);
           /* } catch (InstantiationException e) {
                throw new TrackedRuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new TrackedRuntimeException(e);*/
            }
        }
    }

    public void getQuestList(){

    }

    private void createQuest(String questType){
        Quest quest;
        if (questType.equals("FetchQuest")){
            quest = new FetchQuest();
        } else {
            quest = new HuntQuest();
        }
        questMap.add(quest);
    }
}