/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mechanics.spells;

import android.support.annotation.NonNull;

import com.watabou.noosa.particles.Emitter;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.buffs.Buff;
import com.watabou.pixeldungeon.effects.particles.FlameParticle;

/**
 * Created by mike on 05.09.2017.
 * This file is part of Pixel Dungeon 2.
 */

public class MagicTorch extends Spell{

	MagicTorch() {
		targetingType = SpellHelper.TARGET_SELF;
		magicAffinity = SpellHelper.AFFINITY_COMMON;

		imageIndex = 0;
		duration = 80f;
		spellCost = 1;
	}

	@Override
	public boolean cast(@NonNull Char chr){
		if (super.cast(chr)){
			if (chr.isAlive()) {
				castCallback(chr);
				Buff.affect(chr, com.watabou.pixeldungeon.actors.buffs.Light.class, duration);

				Emitter emitter = chr.getSprite().centerEmitter();
				emitter.start(FlameParticle.FACTORY, 0.2f, 3);
				return true;
			}
		}
		return false;
	}
}
