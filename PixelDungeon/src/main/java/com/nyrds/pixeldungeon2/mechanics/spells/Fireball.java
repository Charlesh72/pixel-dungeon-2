/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mechanics.spells;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.blobs.Blob;
import com.watabou.pixeldungeon.actors.blobs.LiquidFlame;
import com.watabou.pixeldungeon.scenes.GameScene;

/**
 * Created by mike on 05.09.2017.
 * This file is part of Pixel Dungeon 2.
 */

public class Fireball  extends Spell{

	Fireball() {
		targetingType = SpellHelper.TARGET_CELL;
		magicAffinity = SpellHelper.AFFINITY_NECROMANCY;

		name          = "Testing fireball";
		desc          = "Testing fireball";
		imageIndex = 0;

		spellCost = 0;
	}

	@Override
	public boolean cast(Char chr, int cell){

		if(Dungeon.level.cellValid(cell)) {
			LiquidFlame fire = Blob.seed( cell, 50, LiquidFlame.class );
			GameScene.add( fire );
			return true;
		}
		return false;
	}
}
