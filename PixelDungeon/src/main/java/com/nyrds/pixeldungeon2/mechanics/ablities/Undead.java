/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mechanics.ablities;

import com.watabou.pixeldungeon.actors.blobs.ConfusionGas;
import com.watabou.pixeldungeon.actors.blobs.ToxicGas;
import com.watabou.pixeldungeon.actors.buffs.Amok;
import com.watabou.pixeldungeon.actors.buffs.Blindness;
import com.watabou.pixeldungeon.actors.buffs.Paralysis;
import com.watabou.pixeldungeon.actors.buffs.Poison;
import com.watabou.pixeldungeon.actors.buffs.Sleep;
import com.watabou.pixeldungeon.actors.buffs.Terror;
import com.watabou.pixeldungeon.items.weapon.enchantments.Death;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mike on 12.02.2017.
 * This file is part of Pixel Dungeon 2.
 */

public class Undead implements Abilities {

	private static final Set<Class<?>> undead_immunities = new HashSet<>();

	static {
		undead_immunities.add(Paralysis.class);
		undead_immunities.add(ToxicGas.class);
		undead_immunities.add(Terror.class);
		undead_immunities.add(Death.class);
		undead_immunities.add(Amok.class);
		undead_immunities.add(Blindness.class);
		undead_immunities.add(Sleep.class);
		undead_immunities.add(Poison.class);
		undead_immunities.add(ConfusionGas.class);
	}


	@Override
	public Set<Class<?>> immunities() {
		return undead_immunities;
	}

	public static Undead instance = new Undead();
}
