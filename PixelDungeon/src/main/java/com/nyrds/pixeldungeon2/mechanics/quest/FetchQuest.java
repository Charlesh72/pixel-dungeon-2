/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mechanics.quest;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.items.Item;

/**
 * Created by DeadDie on 09.05.2017
 */
public class FetchQuest extends Quest {

    private static Item questItem = null;

    private void setQuestItem(Item item, int quantity) {
        questItem = item;
        questQuantity = quantity;
    }

    private void setQuestItem(Item item) {
        setQuestItem(item, 1);
    }

    private boolean checkForItem(){
        Item item = Dungeon.hero.belongings.getItem(questItem.getClass());
        if ( item != null && item.quantity() >= questQuantity){
            item.removeItemFrom(Dungeon.hero);
            return true;
        } else{
            return false;
        }
    }

}
