/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mechanics.quest;
/**
 * Created by DeadDie on 09.05.2017
 */
public class Quest {

    private static boolean started = false;
    private static boolean completed = false;
    private static boolean turnedIn = false;
    private static int questProgress = 0;
    protected static int questQuantity = 1;

    public Quest(){
    }

    void startQuest(){ started = true; }

    void completeQuest(){
        completed = true;
    }

    void turnInQuest(){
        turnedIn = true;
    }

    public boolean isStarted(){
        return started;
    }

    public boolean isCompleted(){
        return completed;
    }

    public boolean isTurnedIn(){
        return turnedIn;
    }

    public static void reset() {
        started = false;
        completed = false;
        turnedIn = false;
        questProgress = 0;
        questQuantity = 1;
    }

    protected void checkForCompletion(){
        if (questProgress >= questQuantity) {
            completeQuest();
        }
    }

    private void progressQuest(){
        questQuantity++;
    }

    public void setQuestQuantity(int quantity){
        questQuantity = quantity;
    }

}
