/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mechanics.actors;

import com.nyrds.Packable;
import com.nyrds.android.lua.LuaEngine;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.utils.Bundle;

import org.luaj.vm2.LuaTable;

/**
 * Created by mike on 05.11.2017.
 * This file is part of Pixel Dungeon 2.
 */

public class ScriptedActor extends Actor {
	@Packable
	private String sourceFile;

	public ScriptedActor() {}

	public ScriptedActor(String sSourceFile) {
		sourceFile = sSourceFile;
	}

	@Override
	public void restoreFromBundle(Bundle bundle) {
		super.restoreFromBundle(bundle);
	}

	@Override
	protected boolean act() {
		LuaTable actor = LuaEngine.getEngine().call("require",  sourceFile).checktable();

		boolean ret = actor.get("act").call().checkboolean();
		spend((float) actor.get("actionTime").call().checkdouble());

		return ret;
	}

	public void activate() {
		LuaTable actor = LuaEngine.getEngine().call("require",  sourceFile).checktable();
		actor.get("activate").call();
	}
}
