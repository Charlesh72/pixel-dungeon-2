/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.support;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.nyrds.pixeldungeon2.ml.BuildConfig;
import com.nyrds.pixeldungeon2.ml.EventCollector;
import com.watabou.noosa.Game;
import com.watabou.noosa.InterstitialPoint;
import com.watabou.pixeldungeon.utils.Utils;

/**
 * Created by mike on 18.09.2016.
 */
class OwnAds {

	private static final String adTemplate = "<head><meta name=\"viewport\" content=\"user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi\" /></head>" +
			"<div width=100%%>%s</div>" +
			"<div align=\"right\">.</div>";


	private static final String isAdTemplate = "<head><meta name=\"viewport\" content=\"user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi\" /></head>" +
			"<div width=100%% height=100%%>%s</div>" +
			"<div align=\"right\"height=100%%>.</div>";

	static void displayBanner() {
		EventCollector.logEvent("OwnAds", "banner");
		if (BuildConfig.DEBUG) {
			Game.instance().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					LinearLayout layout = Game.instance().getLayout();
					if (layout.getChildCount() == 1) {

						WebView adView = new WebView(Game.instance());

						int adViewHeight = Math.max(50, layout.getHeight() / 10);

						ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, adViewHeight);
						adView.setLayoutParams(params);

						adView.loadDataWithBaseURL(null, Utils.format(adTemplate, "Рекламко"), "text/html", "utf-8", null);
						Game.instance().getLayout().addView(adView, 0);
						Game.setNeedSceneRestart(true);
					}
				}
			});
		}
	}

	static void displayIsAd(final InterstitialPoint work) {
		EventCollector.logEvent("OwnAds", "is");
		if (BuildConfig.DEBUG) {
			Game.instance().runOnUiThread(new Runnable() {

				                              @Override
				                              public void run() {

					                              final AlertDialog.Builder alert = new AlertDialog.Builder(Game.instance());

					                              WebView adView = new WebView(Game.instance());


					                              ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
					                              adView.setLayoutParams(params);

					                              adView.loadDataWithBaseURL(null, Utils.format(isAdTemplate, "Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко Рекламко"), "text/html", "utf-8", null);
					                              alert.setView(adView);
					                              alert.setCustomTitle(null);

					                              final AlertDialog dialog = alert.create();

					                              dialog.setCanceledOnTouchOutside(true);
					                              dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
						                              @Override
						                              public void onCancel(DialogInterface dialog) {
							                              dialog.dismiss();
							                              work.returnToWork(true);
						                              }
					                              });
					                              dialog.show();
				                              }
			                              }
			);
		} else {
			work.returnToWork(true);
		}
	}
}
