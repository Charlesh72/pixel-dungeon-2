/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mobs.icecaves;

import com.nyrds.pixeldungeon2.items.icecaves.IceKey;
import com.nyrds.pixeldungeon2.items.icecaves.WandOfIcebolt;
import com.watabou.pixeldungeon.Badges;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.blobs.ToxicGas;
import com.watabou.pixeldungeon.actors.buffs.Amok;
import com.watabou.pixeldungeon.actors.buffs.Blindness;
import com.watabou.pixeldungeon.actors.buffs.Paralysis;
import com.watabou.pixeldungeon.actors.buffs.Sleep;
import com.watabou.pixeldungeon.actors.buffs.Terror;
import com.watabou.pixeldungeon.actors.mobs.Boss;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.keys.SkeletonKey;
import com.watabou.pixeldungeon.items.weapon.enchantments.Death;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.utils.Random;

public class IceGuardianCore extends Boss {

	public IceGuardianCore() {
		hp(ht(1000));
		exp = 5;
		defenseSkill = 10;

		baseSpeed = 0.5f;
		loot = new WandOfIcebolt().upgrade(1);
		lootChance = 1.0f;

		IMMUNITIES.add( Paralysis.class );
		IMMUNITIES.add( ToxicGas.class );
		IMMUNITIES.add( Terror.class );
		IMMUNITIES.add( Death.class );
		IMMUNITIES.add( Amok.class );
		IMMUNITIES.add( Blindness.class );
		IMMUNITIES.add( Sleep.class );
	}
	
	@Override
	public int damageRoll() {
		return Random.NormalIntRange( 13, 23 );
	}
	
	@Override
	public int attackSkill( Char target ) {
		return 26;
	}
	
	@Override
	public int dr() {
		return 11;
	}

	@Override
	public void die(Object cause) {
		super.die(cause);

		for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
			if (mob instanceof IceGuardian) {
				mob.die(cause);
			}
		}
		Level level = Dungeon.level;
		level.unseal();

		GameScene.bossSlain();
		Dungeon.level.drop( new SkeletonKey(), getPos() ).sprite.drop();
		Dungeon.level.drop( new IceKey(), getPos() ).sprite.drop();

		Badges.validateBossSlain(Badges.Badge.ICE_GUARDIAN_SLAIN);
	}
}