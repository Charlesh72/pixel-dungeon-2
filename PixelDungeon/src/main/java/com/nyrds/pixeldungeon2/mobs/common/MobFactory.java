/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mobs.common;

import com.nyrds.android.util.TrackedRuntimeException;
import com.nyrds.pixeldungeon2.mobs.elementals.AirElemental;
import com.nyrds.pixeldungeon2.mobs.elementals.EarthElemental;
import com.nyrds.pixeldungeon2.mobs.elementals.IceElemental;
import com.nyrds.pixeldungeon2.mobs.elementals.WaterElemental;
import com.nyrds.pixeldungeon2.mobs.guts.MimicAmulet;
import com.nyrds.pixeldungeon2.mobs.guts.Nightmare;
import com.nyrds.pixeldungeon2.mobs.guts.PseudoRat;
import com.nyrds.pixeldungeon2.mobs.guts.SuspiciousRat;
import com.nyrds.pixeldungeon2.mobs.guts.Worm;
import com.nyrds.pixeldungeon2.mobs.guts.YogsBrain;
import com.nyrds.pixeldungeon2.mobs.guts.YogsEye;
import com.nyrds.pixeldungeon2.mobs.guts.YogsHeart;
import com.nyrds.pixeldungeon2.mobs.guts.YogsTeeth;
import com.nyrds.pixeldungeon2.mobs.guts.ZombieGnoll;
import com.nyrds.pixeldungeon2.mobs.icecaves.ColdSpirit;
import com.nyrds.pixeldungeon2.mobs.icecaves.IceGuardian;
import com.nyrds.pixeldungeon2.mobs.icecaves.IceGuardianCore;
import com.nyrds.pixeldungeon2.mobs.icecaves.Kobold;
import com.nyrds.pixeldungeon2.mobs.icecaves.KoboldIcemancer;
import com.nyrds.pixeldungeon2.mobs.necropolis.DeathKnight;
import com.nyrds.pixeldungeon2.mobs.necropolis.DreadKnight;
import com.nyrds.pixeldungeon2.mobs.necropolis.EnslavedSoul;
import com.nyrds.pixeldungeon2.mobs.necropolis.ExplodingSkull;
import com.nyrds.pixeldungeon2.mobs.necropolis.JarOfSouls;
import com.nyrds.pixeldungeon2.mobs.necropolis.Lich;
import com.nyrds.pixeldungeon2.mobs.necropolis.RunicSkull;
import com.nyrds.pixeldungeon2.mobs.necropolis.Zombie;
import com.nyrds.pixeldungeon2.mobs.npc.ArtificerNPC;
import com.nyrds.pixeldungeon2.mobs.npc.BellaNPC;
import com.nyrds.pixeldungeon2.mobs.npc.CagedKobold;
import com.nyrds.pixeldungeon2.mobs.npc.FortuneTellerNPC;
import com.nyrds.pixeldungeon2.mobs.npc.HealerNPC;
import com.nyrds.pixeldungeon2.mobs.npc.LibrarianNPC;
import com.nyrds.pixeldungeon2.mobs.npc.PlagueDoctorNPC;
import com.nyrds.pixeldungeon2.mobs.npc.ServiceManNPC;
import com.nyrds.pixeldungeon2.mobs.npc.TownGuardNPC;
import com.nyrds.pixeldungeon2.mobs.npc.TownsfolkMovieNPC;
import com.nyrds.pixeldungeon2.mobs.npc.TownsfolkNPC;
import com.nyrds.pixeldungeon2.mobs.npc.TownsfolkSilentNPC;
import com.nyrds.pixeldungeon2.mobs.spiders.SpiderEgg;
import com.nyrds.pixeldungeon2.mobs.spiders.SpiderExploding;
import com.nyrds.pixeldungeon2.mobs.spiders.SpiderMind;
import com.nyrds.pixeldungeon2.mobs.spiders.SpiderNest;
import com.nyrds.pixeldungeon2.mobs.spiders.SpiderQueen;
import com.nyrds.pixeldungeon2.mobs.spiders.SpiderServant;
import com.watabou.pixeldungeon.actors.mobs.Acidic;
import com.watabou.pixeldungeon.actors.mobs.Albino;
import com.watabou.pixeldungeon.actors.mobs.Bandit;
import com.watabou.pixeldungeon.actors.mobs.Bat;
import com.watabou.pixeldungeon.actors.mobs.Brute;
import com.watabou.pixeldungeon.actors.mobs.Crab;
import com.watabou.pixeldungeon.actors.mobs.DM300;
import com.watabou.pixeldungeon.actors.mobs.Elemental;
import com.watabou.pixeldungeon.actors.mobs.Eye;
import com.watabou.pixeldungeon.actors.mobs.Gnoll;
import com.watabou.pixeldungeon.actors.mobs.Golem;
import com.watabou.pixeldungeon.actors.mobs.Goo;
import com.watabou.pixeldungeon.actors.mobs.King;
import com.watabou.pixeldungeon.actors.mobs.King.Undead;
import com.watabou.pixeldungeon.actors.mobs.Mimic;
import com.watabou.pixeldungeon.actors.mobs.MimicPie;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.Monk;
import com.watabou.pixeldungeon.actors.mobs.Piranha;
import com.watabou.pixeldungeon.actors.mobs.Rat;
import com.watabou.pixeldungeon.actors.mobs.Scorpio;
import com.watabou.pixeldungeon.actors.mobs.Senior;
import com.watabou.pixeldungeon.actors.mobs.Shadow;
import com.watabou.pixeldungeon.actors.mobs.Shaman;
import com.watabou.pixeldungeon.actors.mobs.Shielded;
import com.watabou.pixeldungeon.actors.mobs.Skeleton;
import com.watabou.pixeldungeon.actors.mobs.Spinner;
import com.watabou.pixeldungeon.actors.mobs.Statue;
import com.watabou.pixeldungeon.actors.mobs.Succubus;
import com.watabou.pixeldungeon.actors.mobs.Swarm;
import com.watabou.pixeldungeon.actors.mobs.Tengu;
import com.watabou.pixeldungeon.actors.mobs.Thief;
import com.watabou.pixeldungeon.actors.mobs.Warlock;
import com.watabou.pixeldungeon.actors.mobs.Wraith;
import com.watabou.pixeldungeon.actors.mobs.Yog;
import com.watabou.pixeldungeon.actors.mobs.Yog.BurningFist;
import com.watabou.pixeldungeon.actors.mobs.Yog.Larva;
import com.watabou.pixeldungeon.actors.mobs.Yog.RottingFist;
import com.watabou.pixeldungeon.actors.mobs.npcs.Ghost.FetidRat;
import com.watabou.pixeldungeon.actors.mobs.npcs.Hedgehog;
import com.watabou.pixeldungeon.actors.mobs.npcs.RatKing;
import com.watabou.utils.Random;

import java.util.HashMap;
import java.util.Map;


public class MobFactory {
	static private Map<String, Class<? extends Mob>> mMobsList;

	static {
		initMobsMap();
	}

	private static void registerMobClass(Class<? extends Mob> mobClass) {
		mMobsList.put(mobClass.getSimpleName(), mobClass);
	}
	
	private static void initMobsMap() {

		mMobsList = new HashMap<>();
		registerMobClass(Rat.class);
		registerMobClass(Albino.class);
		registerMobClass(Gnoll.class);
		registerMobClass(Crab.class);
		registerMobClass(Swarm.class);
		registerMobClass(Thief.class);
		registerMobClass(Skeleton.class);
		registerMobClass(RatKing.class);
		registerMobClass(Goo.class);

		registerMobClass(Shaman.class);
		registerMobClass(Shadow.class);
		registerMobClass(Bat.class);
		registerMobClass(Brute.class);
		registerMobClass(Tengu.class);
		registerMobClass(Bandit.class);

		registerMobClass(SpiderServant.class);
		registerMobClass(SpiderExploding.class);
		registerMobClass(SpiderMind.class);
		registerMobClass(SpiderEgg.class);
		registerMobClass(SpiderNest.class);
		registerMobClass(SpiderQueen.class);

		registerMobClass(Spinner.class);
		registerMobClass(Elemental.class);
		registerMobClass(Monk.class);
		registerMobClass(DM300.class);
		registerMobClass(Shielded.class);

		registerMobClass(AirElemental.class);
		registerMobClass(WaterElemental.class);
		registerMobClass(EarthElemental.class);
		registerMobClass(Warlock.class);
		registerMobClass(Golem.class);
		registerMobClass(Succubus.class);
		registerMobClass(King.class);
		registerMobClass(Undead.class);
		registerMobClass(Senior.class);

		registerMobClass(Eye.class);
		registerMobClass(Scorpio.class);
		registerMobClass(Acidic.class);
		registerMobClass(Yog.class);
		registerMobClass(Larva.class);
		registerMobClass(BurningFist.class);
		registerMobClass(RottingFist.class);

		registerMobClass(FetidRat.class);

		registerMobClass(Wraith.class);
		registerMobClass(Mimic.class);
		registerMobClass(MimicPie.class);
		registerMobClass(Statue.class);
		registerMobClass(Piranha.class);

		registerMobClass(MimicAmulet.class);
		registerMobClass(Worm.class);
		registerMobClass(YogsBrain.class);
		registerMobClass(YogsEye.class);
		registerMobClass(YogsHeart.class);
		registerMobClass(YogsTeeth.class);
		registerMobClass(ZombieGnoll.class);
		registerMobClass(ShadowLord.class);
		registerMobClass(Nightmare.class);
		registerMobClass(SuspiciousRat.class);
		registerMobClass(PseudoRat.class);

		registerMobClass(ArmoredStatue.class);
		registerMobClass(GoldenStatue.class);

		registerMobClass(DeathKnight.class);
		registerMobClass(DreadKnight.class);
		registerMobClass(EnslavedSoul.class);
		registerMobClass(ExplodingSkull.class);
		registerMobClass(JarOfSouls.class);
		registerMobClass(Lich.class);
		registerMobClass(RunicSkull.class);
		registerMobClass(Zombie.class);

		registerMobClass(Crystal.class);

		registerMobClass(Kobold.class);
		registerMobClass(KoboldIcemancer.class);
		registerMobClass(ColdSpirit.class);

		registerMobClass(IceElemental.class);
		registerMobClass(IceGuardian.class);
		registerMobClass(IceGuardianCore.class);

		registerMobClass(Hedgehog.class);
		registerMobClass(HealerNPC.class);
		registerMobClass(TownGuardNPC.class);
		registerMobClass(ServiceManNPC.class);
		registerMobClass(TownsfolkNPC.class);
		registerMobClass(PlagueDoctorNPC.class);
		registerMobClass(TownsfolkMovieNPC.class);
		registerMobClass(TownsfolkSilentNPC.class);
		registerMobClass(BellaNPC.class);
		registerMobClass(LibrarianNPC.class);
		registerMobClass(FortuneTellerNPC.class);
		registerMobClass(CagedKobold.class);
		registerMobClass(ArtificerNPC.class);

		registerMobClass(Deathling.class);
	}
	
	public static Mob mobRandom() {
		try {
			return Random.element(mMobsList.values()).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Mob mobByName(String selectedMobClass) {

		try {
			Class<? extends Mob> mobClass = mMobsList.get(selectedMobClass);
			if (mobClass != null) {
				return mobClass.newInstance();
			} else {
				return new CustomMob(selectedMobClass);
			}
		} catch (Exception e) {
			throw new TrackedRuntimeException(e);
		}
	}
}
