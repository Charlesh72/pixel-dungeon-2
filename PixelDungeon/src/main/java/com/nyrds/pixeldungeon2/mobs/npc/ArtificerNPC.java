/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mobs.npc;

import com.nyrds.pixeldungeon2.mechanics.quest.Quest;
import com.watabou.pixeldungeon.actors.hero.Hero;

public class ArtificerNPC extends ImmortalNPC {

	public ArtificerNPC() {
		quest = new Quest();
	}

	@Override
	public boolean interact(final Hero hero) {
		getSprite().turnTo( getPos(), hero.getPos() );

		if (quest.isTurnedIn()){
			// thanks message
		} else if(quest.isCompleted()) {
			// turn in message and reward
		}

		if (quest.isStarted()){
			// reminder of the quest message
		} else {
			// introduction and instruction to the quest
		}

		return true;
	}
}


