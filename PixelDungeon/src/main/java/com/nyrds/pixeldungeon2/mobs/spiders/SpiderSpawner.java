/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mobs.spiders;

import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.levels.Level;

public class SpiderSpawner {

	static public void spawnQueen(Level level, int position) {
		Mob mob = new SpiderQueen();
		mob.setPos(position);
		mob.setState(mob.WANDERING);
		level.spawnMob(mob);
	}

	static public void spawnEgg(Level level, int position) {
		Mob mob = new SpiderEgg();
		mob.setPos(position);
		mob.setState(mob.SLEEPING);
		level.spawnMob(mob);
	}

	static public void spawnNest(Level level, int position) {
		Mob mob = new SpiderNest();
		mob.setPos(position);
		mob.setState(mob.SLEEPING);
		level.spawnMob(mob);
	}

}
