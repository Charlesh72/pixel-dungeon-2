/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mobs.elementals;

import android.support.annotation.NonNull;

import com.nyrds.pixeldungeon2.mobs.common.IDepthAdjustable;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.blobs.ToxicGas;
import com.watabou.pixeldungeon.actors.buffs.Buff;
import com.watabou.pixeldungeon.actors.buffs.Paralysis;
import com.watabou.pixeldungeon.actors.buffs.Roots;
import com.watabou.pixeldungeon.actors.buffs.Slow;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.food.FrozenCarpaccio;
import com.watabou.utils.Random;

public class IceElemental extends Mob implements IDepthAdjustable {

	public IceElemental() {
		adjustStats(Dungeon.depth);

		loot = new FrozenCarpaccio();
		lootChance = 0.1f;
	}

	public void adjustStats(int depth) {
		hp(ht(depth * 10 + 1));
		defenseSkill = depth * 2 + 1;
		exp = depth + 1;
		maxLvl = depth + 2;
		
		IMMUNITIES.add(Roots.class);
		IMMUNITIES.add(Paralysis.class);
		IMMUNITIES.add(ToxicGas.class);
	}

	@Override
	public int damageRoll() {
		return Random.NormalIntRange(hp() / 6, ht() / 6);
	}

	@Override
	public int attackSkill(Char target) {
		return defenseSkill / 3;
	}

	@Override
	public int dr() {
		return exp;
	}

	@Override
	public int attackProc(@NonNull Char enemy, int damage) {
		//Buff proc
		if (Random.Int(3) == 1){
			if(enemy instanceof Hero) {
				Buff.prolong( enemy, Slow.class, 3 );
			}
		}
		return damage;
	}
}
