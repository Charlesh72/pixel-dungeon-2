/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mobs.elementals;

import android.support.annotation.NonNull;

import com.nyrds.pixeldungeon2.mobs.common.IDepthAdjustable;
import com.watabou.noosa.audio.Sample;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.effects.particles.WindParticle;
import com.watabou.pixeldungeon.items.potions.PotionOfLevitation;
import com.watabou.pixeldungeon.mechanics.Ballistica;
import com.watabou.utils.Random;

public class AirElemental extends Mob implements IDepthAdjustable {

	private static final int maxDistance = 3;

	public AirElemental() {

		adjustStats(Dungeon.depth);

		flying = true;
		
		loot = new PotionOfLevitation();
		lootChance = 0.1f;
	}

	public void adjustStats(int depth) {
		hp(ht(depth * 3 + 1));
		defenseSkill = depth * 2 + 1;
		exp = depth + 1;
		maxLvl = depth + 2;
	}

	@Override
	public int damageRoll() {
		return Random.NormalIntRange(0, ht() / 4);
	}

	@Override
	public int attackSkill(Char target) {
		return defenseSkill * 2;
	}

	@Override
	public int dr() {
		return exp / 5;
	}

	@Override
	protected boolean getCloser(int target) {
		if (getState() == HUNTING && Dungeon.level.distance(getPos(), target) < maxDistance - 1) {
			return getFurther(target);
		}

		return super.getCloser(target);
	}

	@Override
	protected boolean canAttack(Char enemy) {

		if (Dungeon.level.adjacent(getPos(), enemy.getPos())) {
			return false;
		}

		Ballistica.cast(getPos(), enemy.getPos(), true, false);

		for (int i = 1; i < maxDistance; i++) {
			if (Ballistica.trace[i] == enemy.getPos()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int attackProc(@NonNull Char enemy, int damage) {

		Ballistica.cast(getPos(), enemy.getPos(), true, false);

		Char ch;

		for (int i = 1; i < maxDistance; i++) {

			int c = Ballistica.trace[i];

			if ((ch = Actor.findChar(c)) != null && ch instanceof Hero) {
				int next = Ballistica.trace[i + 1];
				if ((Dungeon.level.passable[next] || Dungeon.level.avoid[next])
						&& Actor.findChar(next) == null) {
					ch.move(next);
					ch.getSprite().move(ch.getPos(), next);
					Dungeon.observe();

					ch.getSprite().emitter().burst( WindParticle.FACTORY, 5 );
					ch.getSprite().burst( 0xFF99FFFF, 3 );
					Sample.INSTANCE.play( Assets.SND_MELD );
				} else {
					return damage * 2;
				}
			}
		}
		return damage;
	}

	@Override
	public boolean zap(@NonNull Char enemy) {
		attackProc(enemy, damageRoll());
		super.zap(enemy);
		return true;
	}

}
