/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.mobs.spiders;

import android.util.SparseBooleanArray;

import com.nyrds.pixeldungeon2.mobs.common.MobSpawner;
import com.nyrds.pixeldungeon2.mobs.spiders.sprites.SpiderEggSprite;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.Generator;

public class SpiderEgg extends Mob {

	private static SparseBooleanArray eggsLaid = new SparseBooleanArray();

	public SpiderEgg() {
		spriteClass = SpiderEggSprite.class;

		hp(ht(2));
		defenseSkill = 0;
		baseSpeed = 0f;

		exp = 0;
		maxLvl = 9;

		postpone(20);
		
		loot = Generator.random(Generator.Category.SEED);
		lootChance = 0.2f;
	}

	public static void lay(int pos) {
		eggsLaid.append(pos, true);
		SpiderSpawner.spawnEgg(Dungeon.level, pos);
	}

	public static boolean laid(int pos) {
		return eggsLaid.get(pos, false);
	}
	
	@Override
	protected boolean act() {
		super.act();

		Mob newSpider = MobSpawner.spawnRandomMob(Dungeon.level, getPos());
		if(isPet()) {
			Mob.makePet(newSpider, Dungeon.hero);
		}

		remove();
		eggsLaid.delete(getPos());

		return true;
	}

	@Override
	public boolean canBePet() {
		return false;
	}
}
