/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.windows;

import com.nyrds.android.util.GuiProperties;
import com.nyrds.pixeldungeon2.mechanics.spells.Spell;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.noosa.Text;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.scenes.PixelScene;
import com.watabou.pixeldungeon.ui.RedButton;
import com.watabou.pixeldungeon.ui.TextButton;
import com.watabou.pixeldungeon.ui.Window;

public class WndSpellInfo extends Window {

	protected static final int BTN_HEIGHT	= 18;
	protected static final int WIDTH		= 100;
	protected static final int GAP		= 2;

	protected static final String BTN_BACK = Game.getVar(R.string.Wnd_Button_Back);

	protected String getDesc(){
		return Game.getVar(R.string.WndPortal_Info);
	}

	public WndSpellInfo(final Hero hero, final Spell spell ) {
		super();

		//Title text
		Text tfTitle = PixelScene.createMultiline(spell.name(), GuiProperties.mediumTitleFontSize());
		tfTitle.hardlight(TITLE_COLOR);
		tfTitle.maxWidth(WIDTH - GAP);
		tfTitle.measure();
		tfTitle.x = (WIDTH - tfTitle.width())/2;
		tfTitle.y = GAP;
		add(tfTitle);

		//Info text
		Text info = PixelScene.createMultiline(spell.desc(), GuiProperties.regularFontSize() );
		info.maxWidth(WIDTH);
		info.measure();
		info.y = tfTitle.bottom()+ GAP;
		add( info );

		int buttonY = (int) info.bottom()+ GAP;

		//Back Button
		TextButton btnBack = new RedButton(BTN_BACK) {
			@Override
			protected void onClick() {
				super.onClick();
				hide();
			}
		};

		btnBack.setRect(0, info.bottom() + GAP, WIDTH, BTN_HEIGHT);
		add(btnBack);

		resize( WIDTH, (int) btnBack.bottom() + BTN_HEIGHT / 2);
	}
}
