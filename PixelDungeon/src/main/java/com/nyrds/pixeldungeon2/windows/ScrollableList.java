/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.windows;

import com.watabou.noosa.Gizmo;
import com.watabou.noosa.ui.Component;
import com.watabou.pixeldungeon.ui.IClickable;
import com.watabou.pixeldungeon.ui.ScrollPane;

/**
 * Created by mike on 26.05.2017.
 * This file is part of Pixel Dungeon 2.
 */
public class ScrollableList extends ScrollPane {
	public ScrollableList(Component content) {
		super(content);
	}

	@Override
	public void onClick(float x, float y) {
		int size = content.getLength();
		for (int i = 0; i < size; i++) {
			Gizmo item = content.getMember(i);
			if (item instanceof IClickable) {
				if (((IClickable) item).onClick(x, y)) {
					break;
				}
			}
		}
	}
}
