/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.windows;

import com.nyrds.android.util.GuiProperties;
import com.nyrds.pixeldungeon2.items.necropolis.BlackSkull;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.Game;
import com.watabou.noosa.Text;
import com.watabou.pixeldungeon.scenes.PixelScene;
import com.watabou.pixeldungeon.sprites.ItemSprite;
import com.watabou.pixeldungeon.ui.RedButton;
import com.watabou.pixeldungeon.ui.Window;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.pixeldungeon.utils.Utils;
import com.watabou.pixeldungeon.windows.IconTitle;

public class WndSadGhostNecro extends Window {

	private static final String TXT_TEXT = Game.getVar(R.string.WndSadGhostNecro_Text);
	private static final String TXT_YES = Game.getVar(R.string.WndSadGhostNecro_Yes);
	private static final String TXT_NO = Game.getVar(R.string.WndSadGhostNecro_No);
	private static final String TXT_PERSUADED = Game.getVar(R.string.WndSadGhostNecro_Persuaded);
	
	private static final int WIDTH		= 120;
	private static final int BTN_HEIGHT	= 18;

	private boolean persuade;

	public WndSadGhostNecro() {
		
		super();
		
		IconTitle titlebar = new IconTitle();
		titlebar.icon( new ItemSprite( new BlackSkull()) );
		titlebar.label( Utils.capitalize( Game.getVar(R.string.Necromancy_Title) ) );
		titlebar.setRect( 0, 0, WIDTH, 0 );
		add( titlebar );
		
		Text message = PixelScene.createMultiline( TXT_TEXT, GuiProperties.regularFontSize() );
		message.maxWidth(WIDTH);
		message.measure();
		message.y = titlebar.bottom() + GAP;
		add( message );
		
		RedButton btnWeapon = new RedButton( TXT_YES ) {
			@Override
			protected void onClick() {
				GLog.w( TXT_PERSUADED );
				persuade = true;
				hide();
			}
		};
		btnWeapon.setRect( 0, message.y + message.height() + GAP, WIDTH, BTN_HEIGHT );
		add( btnWeapon );
		
		RedButton btnArmor = new RedButton( TXT_NO ) {
			@Override
			protected void onClick() {
				persuade = false;
				hide();
			}
		};
		btnArmor.setRect( 0, btnWeapon.bottom() + GAP, WIDTH, BTN_HEIGHT );
		add( btnArmor );
		
		resize( WIDTH, (int)btnArmor.bottom() );
	}
	
	public boolean getPersuade() {
		return persuade;
	}
}
