/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.windows;

import com.nyrds.android.util.GuiProperties;
import com.nyrds.pixeldungeon2.items.common.Library;
import com.nyrds.pixeldungeon2.ml.R;
import com.watabou.noosa.CompositeTextureImage;
import com.watabou.noosa.Game;
import com.watabou.noosa.Text;
import com.watabou.noosa.ui.Component;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.scenes.PixelScene;
import com.watabou.pixeldungeon.ui.ListItem;
import com.watabou.pixeldungeon.ui.RedButton;
import com.watabou.pixeldungeon.ui.ScrollPane;
import com.watabou.pixeldungeon.ui.TextButton;
import com.watabou.pixeldungeon.ui.Window;

import java.util.Map;

public class WndLibraryCatalogue extends Window {

	private static final int BTN_HEIGHT = 16;
	private static final int BTN_WIDTH  = 38;
	private static final int GAP        = 4;

	private static final int WIDTH = WndHelper.getLimitedWidth(112);

	public WndLibraryCatalogue(String category, String catalogueName) {
		super();

		int yPos = 0;

		//Title
		Text listTitle = PixelScene.createMultiline(catalogueName, GuiProperties.mediumTitleFontSize());
		listTitle.hardlight(TITLE_COLOR);
		listTitle.maxWidth(WIDTH - GAP * 2);
		listTitle.measure();
		listTitle.x = (WIDTH - listTitle.width()) / 2;
		listTitle.y = 0;

		add(listTitle);

		Component content = new Component();

		Map<String, Integer> knownMap = Library.getKnowledgeMap(category);

		//List
		for (final String entry : knownMap.keySet()) {

			//Button
			Library.EntryHeader entryHeader = Library.infoHeader(category, entry);

			LibraryListItem rb = new LibraryListItem(category, entry, entryHeader);

			rb.setRect(0, yPos, WIDTH, BTN_HEIGHT);
			content.add(rb);

			yPos = (int) rb.bottom() + 1;
		}

		int HEIGHT = WndHelper.getFullscreenHeight() - BTN_HEIGHT * 2;
		int h = Math.min(HEIGHT - GAP, yPos);

		resize(WIDTH, h + BTN_WIDTH);

		content.setSize(WIDTH, yPos);

		ScrollPane list = new ScrollableList(content);

		add(list);

		float topGap = listTitle.height() + GAP;
		float BottomGap = listTitle.bottom() - BTN_HEIGHT / 2;

		list.setRect(0, topGap, WIDTH, HEIGHT - BottomGap);

		//Back Button
		TextButton back = new RedButton(Game.getVar(R.string.Wnd_Button_Back)) {
			@Override
			protected void onClick() {
				super.onClick();
				hide();
				GameScene.show(new WndLibrary());
			}
		};

		back.setRect((WIDTH / 2) - (BTN_WIDTH / 2), (int) list.bottom() + GAP, BTN_WIDTH + GAP, BTN_HEIGHT);

		add(back);
	}

	private static class LibraryListItem extends ListItem {
		private final String finalCategory;
		private final String entryId;

		public LibraryListItem(String category, String entry, Library.EntryHeader desc) {
			finalCategory = category;
			entryId = entry;
			clickable = true;

			if(desc.icon instanceof CompositeTextureImage) {
				sprite.copy((CompositeTextureImage) desc.icon);
			} else {
				sprite.copy(desc.icon);
			}
			label.text(desc.header);
		}

		@Override
		protected void onClick() {
			GameScene.show(Library.infoWindow(finalCategory, entryId));
		}
	}

}