/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.pixeldungeon2.windows;

/**
 * Created by mike on 04.01.2017.
 * This file is part of Pixel Dungeon 2.
 */

public class WndHelper {
	private static float maxWidth, maxHeight;


	public static void update(float mw, float mh) {
		maxWidth = mw;
		maxHeight = mh;
	}

	public static int getFullscreenWidth() {
		return (int) (maxWidth - 14);
	}

	public static int getFullscreenHeight() {
		return (int) (maxHeight - 14);
	}

	public static int getLimitedWidth(int limit) {
		return Math.min(getFullscreenWidth(),limit);
	}
}
