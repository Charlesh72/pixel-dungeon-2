/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.android.util;

import com.nyrds.pixeldungeon2.ml.BuildConfig;
import com.nyrds.pixeldungeon2.support.GooglePlayServices;
import com.watabou.pixeldungeon.PixelDungeon;

/**
 * Created by mike on 04.06.2016.
 */
public class Flavours {

	private static final String GOOGLE_PLAY       = "googlePlay";
	private static final String GOOGLE_PLAY_RETRO = "googlePlayRetro";

	public static boolean haveHats() {
		return ( BuildConfig.FLAVOR.equals(GOOGLE_PLAY) || BuildConfig.FLAVOR.equals(GOOGLE_PLAY_RETRO) )
				&& GooglePlayServices.googlePlayServicesUsable(PixelDungeon.instance());
	}

	public static boolean haveDonations() {
		return (BuildConfig.FLAVOR.equals(GOOGLE_PLAY) || BuildConfig.FLAVOR.equals(GOOGLE_PLAY_RETRO))
				&& GooglePlayServices.googlePlayServicesUsable(PixelDungeon.instance());


	}

	public static boolean haveAds() {
		return BuildConfig.FLAVOR.equals(GOOGLE_PLAY)
				|| BuildConfig.FLAVOR.equals(GOOGLE_PLAY_RETRO);
	}
}
