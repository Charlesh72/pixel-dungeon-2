/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.android.util;

import android.os.AsyncTask;
import android.util.Log;

import com.nyrds.pixeldungeon2.ml.EventCollector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadTask extends AsyncTask<String, Integer, Boolean> {

	private static final String TAG = "DownloadTask";
	private DownloadStateListener m_listener;
	private String                m_url;
	
	public DownloadTask(DownloadStateListener listener) {
		m_listener = listener;
	}

	@Override
	protected Boolean doInBackground(String... args) {
		Boolean result = false;
		m_url = args[0];
		
		System.setProperty("https.protocols", "TLSv1");
		
		publishProgress( 0 );
		
		try {
			URL url   = new URL(m_url);
			File file = new File(args[1]);

			long startTime = System.currentTimeMillis();
			
			Log.d(TAG, "download beginning");
			Log.d(TAG, "download url: " + url);
			Log.d(TAG, "downloaded file name: " + file);
			
			HttpURLConnection ucon = (HttpURLConnection) url.openConnection();

			ucon.setReadTimeout(2500);
			ucon.setInstanceFollowRedirects(true);
			ucon.connect();

			int repCode = ucon.getResponseCode();
			
			if (repCode == HttpURLConnection.HTTP_OK) {
				int bytesTotal = ucon.getContentLength();

				Log.d(TAG, "bytes in file: " + bytesTotal);

				InputStream is = ucon.getInputStream();
				
				FileOutputStream fos = new FileOutputStream(file);

				byte buffer[] = new byte[4096];
				int count;
				int bytesDownloaded = 0;
				while ((count = is.read(buffer)) != -1) {
					fos.write(buffer, 0, count);
					bytesDownloaded += count;
					if(bytesTotal > 0){
						publishProgress( (100 * bytesDownloaded) / bytesTotal);
					}
				}

				fos.close();
				publishProgress( 100 );
				Log.d(TAG,
						"download ready in: "
								+ ((System.currentTimeMillis() - startTime) / 1000)
								+ " sec");
				result = true;
			} else {
				result = false;
			}
		} catch (Exception e) {
			EventCollector.logException(e);
		}

		return result;
	}
	

    protected void onProgressUpdate(Integer... progress) {
    	m_listener.DownloadProgress(m_url, progress[0]);
    }

    protected void onPostExecute(Boolean result) {
    	if(result){
    		Log.d(TAG, "Download ok");
    	} else {
    		Log.d(TAG, "Download failed");
    	}
    	m_listener.DownloadComplete(m_url, result);
    }

}