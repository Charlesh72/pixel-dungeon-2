/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.android.util;

import android.os.AsyncTask;

import java.io.File;

public class UnzipTask extends AsyncTask<String, Integer, Boolean> {

	private UnzipStateListener m_listener;

	public UnzipTask(UnzipStateListener listener) {
		m_listener = listener;
	}

	@Override
	protected Boolean doInBackground(String... args) {

		String zipFile = args[0];

		String tmpDirName = "tmp";

		File tmpDirFile = FileSystem.getExternalStorageFile(tmpDirName);
		if (tmpDirFile.exists()) {
			tmpDirFile.delete();
		}

		if (Unzip.unzip(zipFile, FileSystem.getExternalStorageFile(tmpDirName).getAbsolutePath())) {

			File[] unpackedList = tmpDirFile.listFiles();

			for (File file : unpackedList) {
				if (file.isDirectory()) {

					String modDir = zipFile.substring(0, zipFile.length() - 4);

					if (file.renameTo(new File(modDir))) {
						FileSystem.deleteRecursive(tmpDirFile);
						FileSystem.deleteRecursive(new File(zipFile));
						break;
					} else {
						return false;
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}

	protected void onProgressUpdate(Integer... progress) {
	}

	protected void onPostExecute(Boolean result) {
		m_listener.UnzipComplete(result);
	}

}