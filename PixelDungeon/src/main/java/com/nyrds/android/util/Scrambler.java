/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.android.util;

public class Scrambler {
	static private int k1 = 983, k2 = 991, k3 = 0xAAAAAAAA;
	
	static public void setKeys(int k1, int k2, int k3) {
		Scrambler.k1 = k1;
		Scrambler.k2 = k2;
		Scrambler.k3 = k3;
	}
	
	static public int scramble(int in) {
		return ( (in + k1) * k2 ) ^ k3 ^ UserKey.someValue();
	}
	
	static public int descramble(int in) {
		return ( in ^ UserKey.someValue() ^ k3 ) / k2 - k1;
	}
	
}
