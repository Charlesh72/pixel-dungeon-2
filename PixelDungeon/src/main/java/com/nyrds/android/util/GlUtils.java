/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.android.util;

import android.opengl.GLES20;

/**
 * Created by mike on 24.08.2016.
 */
public class GlUtils {
	static private Boolean npotMipmaps;

	static public boolean isNpotMipmapsSupported() {
		if(npotMipmaps!=null) {
			return npotMipmaps;
		}

		String exts = GLES20.glGetString(GLES20.GL_EXTENSIONS);
		npotMipmaps = exts.indexOf("GL_ARB_texture_non_power_of_two") > 0 || exts.indexOf("GL_OES_texture_npot") > 0;

		return npotMipmaps;
	}
}
