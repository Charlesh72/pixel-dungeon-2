/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.android.util;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Unzip {

	private static final String TAG         = "Unzip";
	private static final int    BUFFER_SIZE = 4096;

	private static void ensureDir(String dir) {
		File f = new File(dir);

		if (!f.exists() || !f.isDirectory()) {
			f.mkdirs();
		}
	}

	static public boolean unzip(InputStream fin, String tgtDir) {
		ensureDir(tgtDir);
		try {
			ZipInputStream zin = new ZipInputStream(fin);
			ZipEntry ze;

			byte data[] = new byte[BUFFER_SIZE];

			while ((ze = zin.getNextEntry()) != null) {
				Log.v(TAG, "Unzipping " + ze.getName());

				if (ze.isDirectory()) {
					ensureDir(tgtDir + "/" + ze.getName());
				} else {

					FileOutputStream fout = new FileOutputStream(tgtDir + "/" + ze.getName());

					int bytesRead;
					while ((bytesRead = zin.read(data)) != -1) {
						fout.write(data, 0, bytesRead);
					}

					zin.closeEntry();
					fout.close();
				}
			}
			zin.close();
		} catch (Exception e) {
			Log.e(TAG, "unzip", e);
			return false;
		}

		return true;
	}

	static public boolean unzip(String zipFile, String tgtDir) {
		try {
			return unzip(new FileInputStream(zipFile), tgtDir);
		} catch (FileNotFoundException e) {
			return false;
		}
	}
}