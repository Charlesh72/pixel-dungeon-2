/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package com.nyrds.android.util;

import android.content.SharedPreferences;
import android.util.Log;

import com.watabou.noosa.Game;

import java.util.UUID;

public class UserKey {
	static UUID userId;

	static Crypter crypter;

	static final String noKey="noKey";

	private static void init() {
		SharedPreferences prefs = Game.instance().getPreferences( Game.MODE_PRIVATE );
		
		String key = prefs.getString("userKey", noKey);
		if(key.equals(noKey)) { 
			userId = UUID.randomUUID();
			
			prefs.edit().putString("userKey", userId.toString()).commit();
		} else {
			userId = UUID.fromString(key);
		}
		
		Log.d("UserKey", userId.toString());
		
		crypter = new Crypter("RPD_UserKey_"+userId.toString());
	}

	public static int someValue(){
		if(userId == null){
			init();
		}

		return (int)userId.getLeastSignificantBits();
	}

	public static String encrypt(String in){
		if(crypter == null){
			init();
		}
		return crypter.encrypt(in);
	}
	
	public static String decrypt(String in){
		if(crypter == null){
			init();
		}
		return crypter.decrypt(in);
	}
}
