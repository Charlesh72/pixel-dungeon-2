/*
 * Pixel Dungeon 2 is a rogue-like dungeon crawler simulator
 *
 *   Copyright © 2017 SandFromGlass
 *   Copyright © 2015-2017 NYRDS
 *   Copyright © 2012-2015 Oleg Dolya
 *
 *   This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */

package net.skoumal.emulatordetector;

import android.os.Build;
import android.util.Log;

public class EmulatorDetector {

	private static final String TAG = "EmulatorDetector";

	private static int rating = -1;

	/**
	 * Detects if app is currenly running on emulator, or real device.
	 * @return true for emulator, false for real devices
	 */
	public static boolean isEmulator() {

		if(rating < 0) { // rating is not calculated yet
			int newRating = 0;

			if(Build.PRODUCT.equals("sdk") ||
					Build.PRODUCT.equals("google_sdk") ||
					Build.PRODUCT.equals("sdk_x86") ||
					Build.PRODUCT.equals("vbox86p")) {
				newRating ++;
			}

			if(Build.MANUFACTURER.equals("unknown") ||
					Build.MANUFACTURER.equals("Genymotion")) {
				newRating ++;
			}

			if(Build.BRAND.equals("generic") ||
					Build.BRAND.equals("generic_x86")) {
				newRating ++;
			}

			if(Build.DEVICE.equals("generic") ||
					Build.DEVICE.equals("generic_x86") ||
					Build.DEVICE.equals("vbox86p")) {
				newRating ++;
			}

			if(Build.MODEL.equals("sdk") ||
					Build.MODEL.equals("google_sdk") ||
					Build.MODEL.equals("Android SDK built for x86")) {
				newRating ++;
			}

			if(Build.HARDWARE.equals("goldfish") ||
					Build.HARDWARE.equals("vbox86")) {
				newRating ++;
			}

			if(Build.FINGERPRINT.contains("generic/sdk/generic") ||
					Build.FINGERPRINT.contains("generic_x86/sdk_x86/generic_x86") ||
					Build.FINGERPRINT.contains("generic/google_sdk/generic") ||
					Build.FINGERPRINT.contains("generic/vbox86p/vbox86p")) {
				newRating ++;
			}

			rating = newRating;
		}

		return rating > 4;
	}

	/**
	 * Returns string with human-readable listing of Build.* parameters used in {@link #isEmulator()} method.
	 * @return all involved Build.* parameters and its values
	 */
	public static String getDeviceListing() {
		return "Build.PRODUCT: " + Build.PRODUCT + "\n" +
				"Build.MANUFACTURER: " + Build.MANUFACTURER + "\n" +
				"Build.BRAND: " + Build.BRAND + "\n" +
				"Build.DEVICE: " + Build.DEVICE + "\n" +
				"Build.MODEL: " + Build.MODEL + "\n" +
				"Build.HARDWARE: " + Build.HARDWARE + "\n" +
				"Build.FINGERPRINT: " + Build.FINGERPRINT;
	}

	/**
	 * Prints all Build.* parameters used in {@link #isEmulator()} method to logcat.
	 */
	public static void logcat() {
		Log.d(TAG, getDeviceListing());
	}

}
