--
-- User: mike
-- Date: 17.11.2017
-- Time: 21:51
-- This file is part of Pixel Dungeon 2.
--

local RPD = require "scripts/lib/commonClasses"

local trap = require"scripts/lib/trap"

return trap.init(
    function (cell, char, data)
        RPD.Chasm:charFall(cell,char)
    end
)
